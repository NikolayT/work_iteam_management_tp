package base;


import commands.Factory;
import commands.FactoryImpl;
import contracts.Person;
import contracts.SubBaseWorkItemInterface;
import contracts.WorkItem;
import enums.PriorityType;
import enums.SeverityType;
import implementations.BugImpl;
import implementations.IDGenerator;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class SubBaseWorkItemTest {

    private final Factory factory = new FactoryImpl();

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
    }

    @Test
    public void person_Should_AssignToWorkItem() {
        List<String> steps = new ArrayList<>();
        WorkItem bug = new BugImpl("testTitle123", "testDescription", steps, PriorityType.MEDIUM, SeverityType.MAJOR);
        Person person = factory.createPerson("testName");
        ((SubBaseWorkItemInterface) bug).assign(person);

        String expectedOutput = "testName";
        Assert.assertEquals(expectedOutput, ((SubBaseWorkItemInterface) bug).getAssignee().getName());
    }

    @Test
    public void unassigningPersonFromWorkItem_Should_MakeAssignerNull() {
        List<String> steps = new ArrayList<>();
        WorkItem bug = new BugImpl("testTitle123", "testDescription", steps, PriorityType.MEDIUM, SeverityType.MAJOR);
        Person person = factory.createPerson("testName");
        ((SubBaseWorkItemInterface) bug).assign(person);
        ((SubBaseWorkItemInterface) bug).unassign();

        Assert.assertNull(((SubBaseWorkItemInterface) bug).getAssignee());
    }
}
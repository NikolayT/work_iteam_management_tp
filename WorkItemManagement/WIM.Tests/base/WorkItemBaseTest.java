package base;

import contracts.Bug;
import contracts.Person;
import contracts.WorkItem;
import enums.PriorityType;
import enums.SeverityType;
import implementations.BugImpl;
import implementations.IDGenerator;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class WorkItemBaseTest {

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
    }

    @Test
    public void toString_Should_ReturnRightMessage(){
        List<String> steps = new ArrayList<>();
        WorkItem bug = new BugImpl("testTitle123","testDescription",steps, PriorityType.MEDIUM, SeverityType.MAJOR);
        Person person = new PersonImpl("testName");
        ((BugImpl) bug).assign(person);
        String additionalInfo = String.format("Steps to reproduce:\n\nPriority: %s\nSeverity: %s\nAssignee: %s",
         ((SubBaseWorkItem)bug).getPriority().getTextValue(), ((BugImpl) bug).getSeverityType().getTextValue(), ((SubBaseWorkItem)bug).getAssignee().getName());
        String expectedOutput = String.format("Type: %s\nID: %s\nTitle: %s\nDescription:\n" +
                        "%s\n%s\nStatus Type: %s\nComments:\nHistory:\n\n",
                bug.getClassName(), bug.getID(), bug.getTitle(), bug.getDescription(), additionalInfo, bug.getStatusType().getTextValue());


        Assert.assertEquals(expectedOutput,bug.toString());

    }

    @Test(expected = IllegalArgumentException.class)
    public void creatingWIWithTitleWithWrongLength_Should_ThrowException(){
        List<String> steps = new ArrayList<>();
        WorkItem bug = new BugImpl("testTitle","testDescription",steps, PriorityType.MEDIUM, SeverityType.MAJOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void creatingWIWithDescriptionWithWrongLength_Should_ThrowException(){
        List<String> steps = new ArrayList<>();
        WorkItem bug = new BugImpl("testTitle1234","test",steps, PriorityType.MEDIUM, SeverityType.MAJOR);
    }

}
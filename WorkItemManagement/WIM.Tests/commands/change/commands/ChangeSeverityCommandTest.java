package commands.change.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.SeverityType;
import enums.StorySizeType;
import implementations.IDGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeSeverityCommandTest {
    private final Factory factory = new FactoryImpl();
    private final Engine engine = new EngineImpl(factory);

    private List<String> parameters;
    @Before
    public void setUp() {
        parameters = new ArrayList<>();
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
    }


    @Test
    public void changeSeverityWithNoParameters_Should_ThrowException(){
        String expectedOutput = "Failed to parse ChangeSeverityCommand parameters";
        Assert.assertEquals(expectedOutput, new ChangeSeverityCommand(engine).execute(parameters));
    }

    @Test
    public void changeSeverityCommandExecute(){
        parameters.add("0000000001");
        parameters.add("Minor");
        List<String> steps = new ArrayList<>();
        steps.add("Step1 test step");
        engine.getWorkItems().add(factory.createBug("titletest123","descriptiontest", steps, PriorityType.MEDIUM, SeverityType.MAJOR ));
        String output = new ChangeSeverityCommand(engine).execute(parameters);
        String expectedOutput =  String.format("Severity type of workitem with id %s, was changed to %s"
                , parameters.get(0),parameters.get(1));

        Assert.assertEquals(expectedOutput,output);
    }
    @Test (expected = IllegalArgumentException.class)
    public void tryingToExecuteCommandWhenIsNotRightWorkItem_Should_ThrowException(){

        parameters.add("0000000001");
        parameters.add("High");
        engine.getWorkItems().add(factory.createStory("titletest13","descriptiontest", PriorityType.MEDIUM, StorySizeType.LARGE));
        String output = new ChangeSeverityCommand(engine).execute(parameters);

    }

}
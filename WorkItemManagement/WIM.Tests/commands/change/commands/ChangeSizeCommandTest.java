package commands.change.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.StorySizeType;
import implementations.IDGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeSizeCommandTest {
    private final Factory factory = new FactoryImpl();
    private final Engine engine = new EngineImpl(factory);

    private List<String> parameters;
    @Before
    public void setUp() {
        parameters = new ArrayList<>();
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
    }

    @Test
    public void changeSizeWithNoParameters_Should_ThrowException(){
        String expectedOutput = "Failed to parse ChangeSizeCommand parameters";
        Assert.assertEquals(expectedOutput, new ChangeSizeCommand(engine).execute(parameters));
    }

    @Test
    public void changeSizeCommandExecute(){

        parameters.add("0000000001");
        parameters.add("Medium");
        engine.getWorkItems().add(factory.createStory("titletest123","descriptiontest", PriorityType.MEDIUM, StorySizeType.LARGE));
        String output = new ChangeSizeCommand(engine).execute(parameters);
        String expectedOutput =  String.format("Size of workitem with id %s, was changed to %s"
                , parameters.get(0),parameters.get(1));

        Assert.assertEquals(expectedOutput,output);
    }
    @Test (expected = IllegalArgumentException.class)
    public void tryingToExecuteCommandWhenIsNotRightWorkItem_Should_ThrowException(){

        parameters.add("0000000001");
        parameters.add("High");
        engine.getWorkItems().add(factory.createFeedBack("titletest123","descriptiontest", 5));
        String output = new ChangeSizeCommand(engine).execute(parameters);

    }

}
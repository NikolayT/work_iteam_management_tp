package commands.change.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.StorySizeType;
import implementations.IDGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeStatusCommandTest {
    private final Factory factory = new FactoryImpl();
    private final Engine engine = new EngineImpl(factory);

    private List<String> parameters;
    @Before
    public void setUp() {
        parameters = new ArrayList<>();
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
    }

    @Test
    public void changeStatusWithNoParameters_Should_ThrowException(){
        String expectedOutput = "Failed to parse ChangeStatusCommand parameters";
        Assert.assertEquals(expectedOutput, new ChangeStatusCommand(engine).execute(parameters));
    }

    @Test
    public void changeStatusCommandExecute(){

        parameters.add("0000000001");
        parameters.add("Done");
        engine.getWorkItems().add(factory.createStory("titletest123","descriptiontest", PriorityType.MEDIUM, StorySizeType.LARGE));
        String output = new ChangeStatusCommand(engine).execute(parameters);
        String expectedOutput =  String.format("Status type of workitem with id %s, was changed to %s"
                , parameters.get(0),parameters.get(1));

        Assert.assertEquals(expectedOutput,output);
    }

}
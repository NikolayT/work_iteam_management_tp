package commands.creation;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Team;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.IDGenerator;
import implementations.TeamImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateBugCommandTest {

    private final Factory factory = new FactoryImpl();
    private final Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
    }

    @Test
    public void createBugWithNoParameters_Should_ThrowException() {
        String expectedOutput = "Failed to parse CreateBugCommand parameters";
        Assert.assertEquals(expectedOutput, new CreateBugCommand(factory, engine).execute(parameters));
    }

    @Test
    public void createBug_Should_ReturnProperMessage() {
        Team team = new TeamImpl("testTeam");
        engine.getTeams().add(team);
        team.createNewBoard("boardName");
        engine.getBoards().add(factory.createBoard("boardName"));
        parameters.add(team.getName());
        parameters.add("boardName");
        parameters.add("titleTesting");
        parameters.add("descriptionTest");
        parameters.add("High");
        parameters.add("Critical");
        parameters.add("Step1 test");
        String expectedOutput = String.format("Bug with ID 0000000001 was created.");

        Assert.assertEquals(expectedOutput, new CreateBugCommand(factory, engine).execute(parameters));
    }

}
package commands.creation;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Team;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class CreateNewBoardInATeamCommandTest {
    private final static String TEAM_NAME = "PeshoTeam";
    private final static String TEST_NAME1 = "board";
    private final Factory factory = new FactoryImpl();
    private final Engine engine = new EngineImpl(factory);
    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
    }

    @Test
    public void createBoardWithNoParameters_Should_ThrowException(){
        String expectedOutput = "Failed to parse CreateNewBoardInATeamCommand parameters";
        Assert.assertEquals(expectedOutput, new CreateNewBoardInATeamCommand(factory, engine).execute(parameters));
    }

    @Test
    public void createBoard_Should_ReturnProperMessage(){
       Team team = factory.createTeam(TEAM_NAME);
       List<String> a = new ArrayList<>();
       a.add(TEAM_NAME);
       String test = new CreateNewTeamCommand(factory,engine).execute(a);
        parameters.add(TEST_NAME1);
        parameters.add("PeshoTeam");
        String expectedOutput = String.format("Board with name %s was created.",TEST_NAME1);

        Assert.assertEquals(expectedOutput,new CreateNewBoardInATeamCommand(factory,engine).execute(parameters));
    }

}
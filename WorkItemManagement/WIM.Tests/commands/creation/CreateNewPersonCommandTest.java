package commands.creation;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateNewPersonCommandTest {
    private final static String TEST_NAME = "Pesho";
    private final Factory factory = new FactoryImpl();
    private final Engine engine = new EngineImpl(factory);
    private List<String> parameters;
    @Before
    public void setUp() {
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
    }

    @Test
    public void createPersonWithNoParameters_Should_ThrowException(){
        String expectedOutput = "Failed to parse CreateNewPersonCommand parameters";
        Assert.assertEquals(expectedOutput, new CreateNewPersonCommand(factory, engine).execute(parameters));
    }

    @Test
    public void createPerson_Should_ReturnProperMessage(){
        parameters.add(TEST_NAME);
        String expectedOutput = String.format("Person with name %s was created.",TEST_NAME);

        Assert.assertEquals(expectedOutput,new CreateNewPersonCommand(factory,engine).execute(parameters));
    }
}
package commands.creation;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Team;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.IDGenerator;
import implementations.TeamImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateNewStoryCommandTest {
    private final Factory factory = new FactoryImpl();
    private final Engine engine = new EngineImpl(factory);

    private List<String> parameters;
    @Before
    public void setUp() {
        parameters = new ArrayList<>();
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
    }

    @Test
    public void createStoryWithNoParameters_Should_ThrowException(){
        String expectedOutput = "Failed to parse CreateNewStoryCommand parameters";
        Assert.assertEquals(expectedOutput, new CreateNewStoryCommand(factory, engine).execute(parameters));
    }

    @Test
    public void createStory_Should_ReturnProperMessage(){
        Team team = new TeamImpl("testTeam");
        engine.getTeams().add(team);
        team.createNewBoard("boardName");
        engine.getBoards().add(factory.createBoard("boardName"));
        parameters.add(team.getName());
        parameters.add("boardName");
        parameters.add("titleTesting");
        parameters.add("descriptionTest");
        parameters.add("High");
        parameters.add("Large");
        String expectedOutput = String.format("Story with ID 0000000001 was created.");

        Assert.assertEquals(expectedOutput,new CreateNewStoryCommand(factory,engine).execute(parameters));
    }

}
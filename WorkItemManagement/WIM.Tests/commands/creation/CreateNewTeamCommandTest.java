package commands.creation;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class CreateNewTeamCommandTest {
    private final static String TEST_NAME = "PeshoTeam";
    private final Factory factory = new FactoryImpl();
    private final Engine engine = new EngineImpl(factory);
    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
    }

    @Test
    public void createTeamWithNoParameters_Should_ThrowException(){
        String expectedOutput = "Failed to parse CreateNewTeamCommand parameters";
        Assert.assertEquals(expectedOutput, new CreateNewTeamCommand(factory, engine).execute(parameters));
    }

    @Test
    public void createTeam_Should_ReturnProperMessage(){
        parameters.add(TEST_NAME);
        String expectedOutput = String.format("Team with name %s was created.",TEST_NAME);

        Assert.assertEquals(expectedOutput,new CreateNewTeamCommand(factory,engine).execute(parameters));
    }

}
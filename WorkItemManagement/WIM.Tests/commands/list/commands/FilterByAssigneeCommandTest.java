package commands.list.commands;

import commands.Factory;
import commands.FactoryImpl;
import contracts.SubBaseWorkItemInterface;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.StorySizeType;
import implementations.IDGenerator;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class FilterByAssigneeCommandTest {

    private Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
        engine.getWorkItems().clear();
    }

    @Test
    public void filterByAssigneeCommandWhenThereIsNoAssignee_Should_ReturnProperMessage() {
        parameters.add("Gosho");
        engine.getWorkItems().add(factory.createStory("titletest123", "descriptiontest",
                PriorityType.MEDIUM, StorySizeType.LARGE));

        String output = new FilterByAssigneeCommand(engine).execute(parameters);
        String expectedOutput = "There are no workitems with this assignee";

        Assert.assertEquals(expectedOutput, output);
    }

    @Test
    public void filterByExistingAssignee_Should_ReturnProperMessage() {
        parameters.add("testName");
        engine.getWorkItems().add(factory.createStory("titletest123", "descriptiontest",
                PriorityType.MEDIUM, StorySizeType.LARGE));

        ((SubBaseWorkItemInterface) engine.getWorkItems().get(0)).assign(factory.createPerson("testName"));
        String output = new FilterByAssigneeCommand(engine).execute(parameters);
        String expectedOutput = engine.getWorkItems().get(0).toString();

        Assert.assertEquals(expectedOutput, output);
    }
}
package commands.list.commands;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Person;
import contracts.SubBaseWorkItemInterface;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.SeverityType;
import enums.StatusType;
import enums.StorySizeType;
import implementations.IDGenerator;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterByStatusAndAssigneeTest {

    private Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    private void createWorkItems() {
        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.MAJOR));
        engine.getWorkItems().add(factory.createBug("titleTesting2", "descriptionTest2", new ArrayList<>(),
                PriorityType.MEDIUM, SeverityType.CRITICAL));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting3", "descriptionTest3", 5));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting4", "descriptionTest4", 5));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting5", "descriptionTest5", 5));
        engine.getWorkItems().add(factory.createStory("titleTesting6", "descriptionTest6",
                PriorityType.LOW, StorySizeType.LARGE));

        Person assignee = factory.createPerson("assignee");

        engine.getWorkItems().get(0).changeStatus(StatusType.FIXED);
        engine.getWorkItems().get(1).changeStatus(StatusType.ACTIVE);
        engine.getWorkItems().get(2).changeStatus(StatusType.NEW);
        engine.getWorkItems().get(3).changeStatus(StatusType.DONE);
        engine.getWorkItems().get(4).changeStatus(StatusType.SCHEDULED);
        engine.getWorkItems().get(5).changeStatus(StatusType.DONE);

        ((SubBaseWorkItemInterface) engine.getWorkItems().get(0)).assign(assignee);
        ((SubBaseWorkItemInterface) engine.getWorkItems().get(5)).assign(assignee);
    }

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
        engine.getWorkItems().clear();
    }

    @Test
    public void filterWorkItemsByStatusAndAssignee_Should_ReturnProperMessage() {
        createWorkItems();
        String expectedOutput = String.format("%s", engine.getWorkItems().get(0));
        parameters.add("fixed");
        parameters.add("assignee");

        Assert.assertEquals(expectedOutput, new FilterByStatusAndAsigneeCommand(engine).execute(parameters));
    }

    @Test
    public void filterWorkItemsByStatusAndNonExistingAssignee_Should_ReturnProperMessage() {
        createWorkItems();
        String expectedOutput = "No workitems with this specification found";
        parameters.add("fixed");
        parameters.add("invalidName");

        Assert.assertEquals(expectedOutput, new FilterByStatusAndAsigneeCommand(engine).execute(parameters));
    }
}

package commands.list.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.SeverityType;
import enums.StatusType;
import enums.StorySizeType;
import implementations.IDGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterByStatusCommandTest {

    private final Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    private void createWorkItems() {
        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.MAJOR));
        engine.getWorkItems().add(factory.createBug("titleTesting2", "descriptionTest2", new ArrayList<>(),
                PriorityType.MEDIUM, SeverityType.CRITICAL));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting3", "descriptionTest3", 5));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting4", "descriptionTest4", 5));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting5", "descriptionTest5", 5));
        engine.getWorkItems().add(factory.createStory("titleTesting6", "descriptionTest6",
                PriorityType.LOW, StorySizeType.LARGE));

        engine.getWorkItems().get(0).changeStatus(StatusType.FIXED);
        engine.getWorkItems().get(1).changeStatus(StatusType.ACTIVE);
        engine.getWorkItems().get(2).changeStatus(StatusType.NEW);
        engine.getWorkItems().get(3).changeStatus(StatusType.DONE);
        engine.getWorkItems().get(4).changeStatus(StatusType.SCHEDULED);
        engine.getWorkItems().get(5).changeStatus(StatusType.DONE);
    }

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
    }

    @Test
    public void filterWorkItemsByStatus_Should_ReturnProperMessage() {
        createWorkItems();
        String expectedOutput = String.format("%s%s", engine.getWorkItems().get(3), engine.getWorkItems().get(5));
        parameters.add("done");

        Assert.assertEquals(expectedOutput, new FilterByStatusListCommand(engine).execute(parameters));
    }

    @Test
    public void filterWorkItemsByInvalidStatus_Should_ReturnProperMessage() {
        createWorkItems();
        String expectedOutput = "There are no workitems with this status";
        parameters.add("invalid");

        Assert.assertEquals(expectedOutput, new FilterByStatusListCommand(engine).execute(parameters));
    }

    @Test
    public void filterEmptyWorkItemListByStatus_Should_ReturnProperMessage() {
        String expectedOutput = "There are no workitems with this status";
        parameters.add("Fixed");

        Assert.assertEquals(expectedOutput, new FilterByStatusListCommand(engine).execute(parameters));
    }
}

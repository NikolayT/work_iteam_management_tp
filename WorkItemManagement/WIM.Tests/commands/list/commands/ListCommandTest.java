package commands.list.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.SeverityType;
import enums.StorySizeType;
import implementations.IDGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListCommandTest {

    private final Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    private void createWorkItems() {
        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.MAJOR));
        engine.getWorkItems().add(factory.createBug("titleTesting2", "descriptionTest2", new ArrayList<>(),
                PriorityType.MEDIUM, SeverityType.CRITICAL));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting3", "descriptionTest3", 5));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting4", "descriptionTest4", 5));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting5", "descriptionTest5", 5));
        engine.getWorkItems().add(factory.createStory("titleTesting6", "descriptionTest6",
                PriorityType.LOW, StorySizeType.LARGE));
    }

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
    }

    @Test
    public void filterOnlyTheBugs_Should_ReturnProperMessage() {
        createWorkItems();
        String expectedOutput = String.format("%s%s", engine.getWorkItems().get(0), engine.getWorkItems().get(1));
        parameters.add("bug");

        Assert.assertEquals(expectedOutput, new ListCommand(engine).execute(parameters));
    }

    @Test
    public void filterOnlyTheStories_Should_ReturnProperMessage() {
        createWorkItems();
        String expectedOutput = String.format("%s", engine.getWorkItems().get(5));
        parameters.add("STORY");

        Assert.assertEquals(expectedOutput, new ListCommand(engine).execute(parameters));
    }

    @Test
    public void filterOnlyTheFeedback_Should_ReturnProperMessage() {
        createWorkItems();
        String expectedOutput = String.format("%s%s%s",
                engine.getWorkItems().get(2), engine.getWorkItems().get(3), engine.getWorkItems().get(4));
        parameters.add("FeedBack");

        Assert.assertEquals(expectedOutput, new ListCommand(engine).execute(parameters));
    }

    @Test
    public void listAllWorkItems_Should_ReturnProperMessage() {
        createWorkItems();
        StringBuilder workItemsList = new StringBuilder();
        engine.getWorkItems()
                .forEach(workItem -> workItemsList.append(workItem.toString()));

        parameters.add("ALL");

        Assert.assertEquals(workItemsList.toString(), new ListCommand(engine).execute(parameters));
    }

    @Test
    public void listWorkItemsOfUnknownType_Should_ReturnProperMessage() {
        createWorkItems();
        parameters.add("Invalid");
        String expectedOutput = "There are no workitems of this type";

        Assert.assertEquals(expectedOutput, new ListCommand(engine).execute(parameters));
    }

    @Test
    public void listWorkItemsInEmptyList_Should_ReturnProperMessage() {
        parameters.add("All");
        String expectedOutput = "There are no workitems";

        Assert.assertEquals(expectedOutput, new ListCommand(engine).execute(parameters));
    }
}

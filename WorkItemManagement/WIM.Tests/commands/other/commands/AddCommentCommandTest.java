package commands.other.commands;

import commands.adding.commands.AddCommentCommand;
import commands.Factory;
import commands.FactoryImpl;
import contracts.Person;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.IDGenerator;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddCommentCommandTest {

    private final static String WORK_ITEM_ID = "0000000001";
    private final static String COMMENT_CONTENT = "test-content";
    private final static String PERSON_NAME = "testPerson";

    private final Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getTeams().clear();
        engine.getPeople().clear();
        engine.getWorkItems().clear();
        engine.getBoards().clear();
        engine.getComments().clear();
        PersonImpl.clearNames();
    }

    @Test
    public void addingCommentToWorkItem_Should_ReturnProperMessage() {
        parameters.add(WORK_ITEM_ID);
        parameters.add(COMMENT_CONTENT);
        parameters.add(PERSON_NAME);

        Person person = factory.createPerson(PERSON_NAME);
        engine.getPeople().add(person);
        engine.getWorkItems().add(factory.createFeedBack("testtitleee", "testdescription", 5));
        engine.getTeams().add(factory.createTeam("testTeam"));
        engine.getTeams().get(0).addPerson(person);

        String expectedOutput = "testPerson added a comment for workitem with ID: 0000000001";
        Assert.assertEquals(expectedOutput, new AddCommentCommand(factory, engine).execute(parameters));
    }

    @Test
    public void addingCommentToNonExistingWorkItem_Should_ReturnProperMessage() {
        parameters.add(WORK_ITEM_ID);
        parameters.add(COMMENT_CONTENT);
        parameters.add(PERSON_NAME);

        Person person = factory.createPerson(PERSON_NAME);
        engine.getPeople().add(person);
        engine.getTeams().add(factory.createTeam("testTeam"));
        engine.getTeams().get(0).addPerson(person);

        String expectedOutput = "There is no workitem with ID: 0000000001";
        Assert.assertEquals(expectedOutput, new AddCommentCommand(factory, engine).execute(parameters));
    }

    @Test
    public void addingCommentFromNonExistingPersonToWorkItem_Should_ReturnProperMessage() {
        parameters.add(WORK_ITEM_ID);
        parameters.add(COMMENT_CONTENT);
        parameters.add(PERSON_NAME);

        engine.getWorkItems().add(factory.createFeedBack("testtitleee", "testdescription", 5));
        engine.getTeams().add(factory.createTeam("testTeam"));

        String expectedOutput = "Person with name testPerson does not exist";
        Assert.assertEquals(expectedOutput, new AddCommentCommand(factory, engine).execute(parameters));
    }
}

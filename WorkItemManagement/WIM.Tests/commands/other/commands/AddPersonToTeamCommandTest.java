package commands.other.commands;

import commands.adding.commands.AddPersonToTeam;
import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddPersonToTeamCommandTest {

    private final static String PERSON_NAME = "personName";
    private final static String TEAM_NAME = "teamName";

    private final Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        engine.getTeams().clear();
        engine.getPeople().clear();
        PersonImpl.clearNames();
    }

    @Test
    public void addingPersonToTeamCommand_Should_ReturnProperMessage() {
        parameters.add(PERSON_NAME);
        parameters.add(TEAM_NAME);
        engine.getTeams().add(factory.createTeam(TEAM_NAME));
        engine.getPeople().add(factory.createPerson(PERSON_NAME));
        String expectedOutput = String.format("Person with name %s was added to team with name %s.", PERSON_NAME, TEAM_NAME);

        Assert.assertEquals(expectedOutput, new AddPersonToTeam(engine).execute(parameters));
    }

    @Test
    public void addingPersonToNonExistingTeam_Should_ReturnProperMessage() {
        parameters.add(PERSON_NAME);
        parameters.add(TEAM_NAME);
        engine.getPeople().add(factory.createPerson(PERSON_NAME));
        String expectedOutput = String.format("Team with name %s does not exist", TEAM_NAME);

        Assert.assertEquals(expectedOutput, new AddPersonToTeam(engine).execute(parameters));
    }

    @Test
    public void addingNonExistingPersonInTeam_Should_ReturnProperMessage() {
        parameters.add(PERSON_NAME);
        parameters.add(TEAM_NAME);
        engine.getTeams().add(factory.createTeam(TEAM_NAME));
        String expectedOutput = String.format("Person with name %s does not exist", PERSON_NAME);

        Assert.assertEquals(expectedOutput, new AddPersonToTeam(engine).execute(parameters));
    }
}

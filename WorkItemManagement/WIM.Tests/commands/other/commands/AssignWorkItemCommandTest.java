package commands.other.commands;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Person;
import contracts.WorkItem;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.SeverityType;
import implementations.IDGenerator;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AssignWorkItemCommandTest {

    private final static String WORKITEM_ID = "0000000001";
    private final static String PERSON_NAME = "testName";

    private final Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    private void setUpTeam() {
        engine.getTeams().add(factory.createTeam("testTeam"));
        WorkItem w = factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL);
        engine.getWorkItems().add(w);
        Person p = factory.createPerson(PERSON_NAME);
        engine.getPeople().add(p);
        engine.getTeamByName("testTeam").addPerson(p);
        engine.getTeamByName("testTeam").createNewBoard("boardName");
        engine.getTeamByName("testTeam").addWorkItemOnBoard("boardName", w);
    }

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
        engine.getPeople().clear();
        PersonImpl.clearNames();
    }

    @Test
    public void assignWorkItemWhenThereAreNoPeople_Should_ReturnProperMessage() {
        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL));
        parameters.add(WORKITEM_ID);
        parameters.add(PERSON_NAME);

        String expectedOutput = "There are no registered people";
        Assert.assertEquals(expectedOutput, new AssignWorkItemCommand(engine, true).execute(parameters));
    }

    @Test
    public void assignWorkItemWhenThereAreNoWorkItems_Should_ReturnProperMessage() {
        parameters.add(WORKITEM_ID);
        parameters.add(PERSON_NAME);
        engine.getPeople().add(factory.createPerson(PERSON_NAME));

        String expectedOutput = "There are no workitems created";
        Assert.assertEquals(expectedOutput, new AssignWorkItemCommand(engine, true).execute(parameters));
    }

    @Test
    public void assignWorkItemToPerson_Should_ReturnProperMessage() {
        parameters.add(WORKITEM_ID);
        parameters.add(PERSON_NAME);
        setUpTeam();

        String expectedOutput = "Person got an item from the board with title: titleTesting";
        Assert.assertEquals(expectedOutput, new AssignWorkItemCommand(engine, true).execute(parameters));
    }

    @Test
    public void assignWorkItemToPersonThatIsNotOnSameBoard_Should_ReturnProperMessage() {
        parameters.add(WORKITEM_ID);
        parameters.add(PERSON_NAME);

        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL));
        engine.getPeople().add(factory.createPerson(PERSON_NAME));
        String firstAssign = new AssignWorkItemCommand(engine, true).execute(parameters);

        String expectedOutput = "This workitem is not on board from the same team as person";
        Assert.assertEquals(expectedOutput, new AssignWorkItemCommand(engine, true).execute(parameters));
    }

    @Test
    public void assignNonAssignableWorkItem_Should_ReturnProperMessage() {
        parameters.add(WORKITEM_ID);
        parameters.add(PERSON_NAME);

        engine.getWorkItems().add(factory.createFeedBack("titleTesting", "descriptionTest", 4));
        engine.getPeople().add(factory.createPerson(PERSON_NAME));

        String expectedOutput = "Workitem of type Feedback cannot have an assigner";
        Assert.assertEquals(expectedOutput, new AssignWorkItemCommand(engine, true).execute(parameters));
    }

    @Test
    public void assignWorkItemToNonExistingPerson_Should_ReturnProperMessage() {
        parameters.add(WORKITEM_ID);
        parameters.add("invalidName");

        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL));
        engine.getPeople().add(factory.createPerson(PERSON_NAME));

        String expectedOutput = "Person with name invalidName does not exist";
        Assert.assertEquals(expectedOutput, new AssignWorkItemCommand(engine, true).execute(parameters));
    }

    @Test
    public void assignNonExistingWorkItemPerson_Should_ReturnProperMessage() {
        parameters.add("invalid");
        parameters.add(PERSON_NAME);

        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL));
        engine.getPeople().add(factory.createPerson(PERSON_NAME));

        String expectedOutput = "There is no workitem with ID: invalid";
        Assert.assertEquals(expectedOutput, new AssignWorkItemCommand(engine, true).execute(parameters));
    }

    @Test
    public void unassignWorkItemThatIsNotAssigned_Should_ReturnProperMessage() {
        parameters.add(WORKITEM_ID);
        parameters.add(PERSON_NAME);

        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL));
        engine.getPeople().add(factory.createPerson(PERSON_NAME));

        String expectedOutput = "testName does not have this work item";
        Assert.assertEquals(expectedOutput, new AssignWorkItemCommand(engine, false).execute(parameters));
    }

    @Test
    public void unassignWorkItemFromPersonInDifferentTeam_Should_ReturnProperMessage() {
        parameters.add(WORKITEM_ID);
        parameters.add(PERSON_NAME);

        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL));
        engine.getPeople().add(factory.createPerson(PERSON_NAME));
        String assign = new AssignWorkItemCommand(engine, true).execute(parameters);

        String expectedOutput = "testName does not have this work item";
        Assert.assertEquals(expectedOutput, new AssignWorkItemCommand(engine, false).execute(parameters));
    }
}
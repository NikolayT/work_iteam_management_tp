package commands.show.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllPeopleCommandTest {

    private Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
    }
    @Test
    public void showAllPeopleCommandWhenThereAreNoPeople_Should_ReturnProperMessage(){

        String expectedOutput = "There are no registered people.";
        Assert.assertEquals(expectedOutput,new ShowAllPeopleCommand(engine).execute(parameters));
    }

    @Test
    public void showAllPeopleCommand_Should_ReturnProperMessage(){
       engine.getPeople().add( factory.createPerson("Pesho"));
        String expectedOutput = "Person: Pesho\n";
        Assert.assertEquals(expectedOutput,new ShowAllPeopleCommand(engine).execute(parameters));
    }

}
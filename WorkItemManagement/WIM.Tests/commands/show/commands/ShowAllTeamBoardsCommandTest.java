package commands.show.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllTeamBoardsCommandTest {
    private Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
    }

    @Test
    public void showAllTeamBoardsWhenNoBoardsCreated_Should_ThrowException(){
        parameters.add("testTeam");
        engine.getTeams().add(factory.createTeam("testTeam"));
        String output = "There are no boards added yet.";

        Assert.assertEquals(output,new ShowAllTeamBoardsCommand(engine).execute(parameters));
    }

    @Test
    public void showAllTeamBoards_Should_ThrowException(){
        parameters.add("testTeam");
        engine.getTeams().add(factory.createTeam("testTeam"));
        engine.getBoards().add(factory.createBoard("testBoard"));
        engine.getTeams().get(0).createNewBoard("firstBoard");
        String output = "Team Boards:\nfirstBoard has no workitems yet";

        Assert.assertEquals(output,new ShowAllTeamBoardsCommand(engine).execute(parameters));
    }
}
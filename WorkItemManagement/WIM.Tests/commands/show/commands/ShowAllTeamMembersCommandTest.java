package commands.show.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllTeamMembersCommandTest {

    private Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
    }

    @Test
    public void showAllTeamMembersWhenNoMembersCreated_Should_ReturnProperMessage(){
        parameters.add("testTeam");
        engine.getTeams().add(factory.createTeam("testTeam"));
        String expectedOutput = "There are no members added yet.";

        Assert.assertEquals(expectedOutput,new ShowAllTeamMembersCommand(engine).execute(parameters));
    }

    @Test
    public void showAllTeamMembers_Should_ReturnProperMessage(){
        parameters.add("testTeam");
        engine.getTeams().add(factory.createTeam("testTeam"));
        engine.getPeople().add(factory.createPerson("Pesho"));
        engine.getTeams().get(0).addPerson(engine.getPeople().get(0));
        String expectedOutput = "Team Members:\n Pesho\n";

        Assert.assertEquals(expectedOutput,new ShowAllTeamMembersCommand(engine).execute(parameters));
    }
}
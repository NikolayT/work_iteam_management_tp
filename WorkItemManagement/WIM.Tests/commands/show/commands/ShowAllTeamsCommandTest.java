package commands.show.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllTeamsCommandTest {
    private Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
    }

    @Test
    public void showAllTeamsWhenNoTeamsCreated_Should_ReturnProperMessage(){
        String expectedOutput = "There are no registered teams.";

        Assert.assertEquals(expectedOutput,new ShowAllTeamsCommand(engine).execute(parameters));
    }

    @Test
    public void showAllTeamsCommand(){
        parameters.add("testTeam");
        engine.getTeams().add(factory.createTeam("testTeam"));
        String expectedOutput = "testTeam\nThere are no members added yet.There are no boards added yet.";

        Assert.assertEquals(expectedOutput,new ShowAllTeamsCommand(engine).execute(parameters));
    }
}
package commands.show.commands;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Board;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.SeverityType;
import implementations.IDGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowBoardActivityCommandTest {

    private final static String BOARD_NAME = "testBoard";

    private final Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
        engine.getBoards().clear();
    }

    private void setUpBoard() {
        Board board = factory.createBoard(BOARD_NAME);
        engine.getBoards().add(board);
        board.createNewWorkItem(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.MAJOR));
    }

    @Test
    public void givenInvalidNumberOfParametersToCommand_Should_ReturnProperMessage() {
        parameters.add("command1");
        parameters.add("command2");

        String expectedOutput = "Failed to parse ShowBoardsActivityCommand parameters";
        Assert.assertEquals(expectedOutput, new ShowBoardsActivityCommand(engine).execute(parameters));
    }

    @Test
    public void showingActivityOfExistingBoard_Should_ReturnProperMessage() {
        parameters.add(BOARD_NAME);
        setUpBoard();

        String expectedOutput = String.format("%s activity history:\n Bug added to the board\n", BOARD_NAME);
        Assert.assertEquals(expectedOutput, new ShowBoardsActivityCommand(engine).execute(parameters));
    }

    @Test
    public void showingActivityOfNonExistingBoard_Should_ReturnProperMessage() {
        parameters.add("InvalidName");
        setUpBoard();

        String expectedOutput = "Board with name InvalidName does not belong to any board";
        Assert.assertEquals(expectedOutput, new ShowBoardsActivityCommand(engine).execute(parameters));
    }

    @Test
    public void showingActivityOfBoardWithNoActivity_Should_ReturnProperMessage() {
        parameters.add(BOARD_NAME);
        engine.getBoards().add(factory.createBoard(BOARD_NAME));

        String expectedOutput = "testBoard has no activity yet";
        Assert.assertEquals(expectedOutput, new ShowBoardsActivityCommand(engine).execute(parameters));
    }
}

package commands.show.commands;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Person;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.IDGenerator;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowPersonActivityCommandTest {

    private final static String PERSON_NAME = "testPerson";
    private final static String TEAM_NAME = "testTeam";

    private final Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getPeople().clear();
        engine.getTeams().clear();
        PersonImpl.clearNames();
    }

    private void setUpPerson() {
        Person person = factory.createPerson(PERSON_NAME);
        engine.getPeople().add(person);
        engine.getTeams().add(factory.createTeam(TEAM_NAME));
        engine.getTeams().get(0).addPerson(person);
    }

    @Test
    public void givenInvalidNumberOfParametersToCommand_Should_ReturnProperMessage() {
        parameters.add("command1");
        parameters.add("command2");

        String expectedOutput = "Failed to parse ShowPersonActivityCommand parameters";
        Assert.assertEquals(expectedOutput, new ShowPersonActivityCommand(engine).execute(parameters));
    }

    @Test
    public void showingActivityOfExistingPerson_Should_ReturnProperMessage() {
        parameters.add(PERSON_NAME);
        setUpPerson();

        String expectedOutput = String.format("%s activity history:\n%s was added to team - %s\n",
                PERSON_NAME, PERSON_NAME, TEAM_NAME);
        Assert.assertEquals(expectedOutput, new ShowPersonActivityCommand(engine).execute(parameters));
    }

    @Test
    public void showingActivityOfNonExistingPerson_Should_ReturnProperMessage() {
        parameters.add("InvalidName");
        setUpPerson();

        String expectedOutput = "Person with name InvalidName does not exist";
        Assert.assertEquals(expectedOutput, new ShowPersonActivityCommand(engine).execute(parameters));
    }

    @Test
    public void showingActivityOfPersonWithNoActivity_Should_ReturnProperMessage() {
        parameters.add(PERSON_NAME);
        engine.getPeople().add(factory.createPerson(PERSON_NAME));

        String expectedOutput = "testPerson has no activity yet";
        Assert.assertEquals(expectedOutput, new ShowPersonActivityCommand(engine).execute(parameters));
    }
}

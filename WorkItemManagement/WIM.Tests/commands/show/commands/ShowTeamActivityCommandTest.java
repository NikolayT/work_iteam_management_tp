package commands.show.commands;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Person;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import implementations.IDGenerator;
import implementations.PersonImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowTeamActivityCommandTest {

    private final static String PERSON_NAME = "testPerson";
    private final static String TEAM_NAME = "testTeam";

    private final Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private List<String> parameters;

    @Before
    public void setUp() {
        parameters = new ArrayList<>();
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getPeople().clear();
        engine.getTeams().clear();
        PersonImpl.clearNames();
    }

    private void setUpTeam() {
        Person person = factory.createPerson(PERSON_NAME);
        engine.getPeople().add(person);
        engine.getTeams().add(factory.createTeam(TEAM_NAME));
        engine.getTeams().get(0).addPerson(person);
    }

    @Test
    public void givenInvalidNumberOfParametersToCommand_Should_ReturnProperMessage() {
        parameters.add("command1");
        parameters.add("command2");

        String expectedOutput = "Failed to parse ShowTeamActivityCommand parameters";
        Assert.assertEquals(expectedOutput, new ShowTeamActivityCommand(engine).execute(parameters));
    }

    @Test
    public void showingActivityOfExistingTeam_Should_ReturnProperMessage() {
        parameters.add(TEAM_NAME);
        setUpTeam();

        String expectedOutput = String.format("%s activity:\n%s activity history:\n%s was added to team - %s\n",
                TEAM_NAME, PERSON_NAME, PERSON_NAME, TEAM_NAME);
        Assert.assertEquals(expectedOutput, new ShowTeamActivityCommand(engine).execute(parameters));
    }

    @Test
    public void showingActivityOfNonExistingTeam_Should_ReturnProperMessage() {
        parameters.add("InvalidName");
        setUpTeam();

        String expectedOutput = "Team with name InvalidName does not exist";
        Assert.assertEquals(expectedOutput, new ShowTeamActivityCommand(engine).execute(parameters));
    }

    @Test
    public void showingActivityOfTeamWithNoActivity_Should_ReturnProperMessage() {
        parameters.add(TEAM_NAME);
        engine.getTeams().add(factory.createTeam(TEAM_NAME));

        String expectedOutput = "testTeam has no activity yet\n";
        Assert.assertEquals(expectedOutput, new ShowTeamActivityCommand(engine).execute(parameters));
    }
}

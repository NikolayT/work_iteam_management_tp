package commands.sort.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.SeverityType;
import enums.StorySizeType;
import implementations.IDGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class SortByPriorityCommand {
    private final Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private void createWorkItems() {
        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.MEDIUM, SeverityType.MAJOR));
        engine.getWorkItems().add(factory.createBug("titleTesting2", "descriptionTest2", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL));
        engine.getWorkItems().add(factory.createStory("titleTesting6", "descriptionTest6",
                PriorityType.LOW, StorySizeType.LARGE));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting3", "descriptionTest3", 2));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting4", "descriptionTest4", 5));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting5", "descriptionTest5", 1));
        engine.getWorkItems().add(factory.createStory("titleTesting6", "descriptionTest6",
                PriorityType.LOW, StorySizeType.SMALL));
        engine.getWorkItems().add(factory.createBug("titleTesting2", "descriptionTest2", new ArrayList<>(),
                PriorityType.MEDIUM, SeverityType.MINOR));
    }

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
    }

    @Test
    public void sortByPriorityCommand_Should_SortWorkItemsProperly() {
        createWorkItems();
        String expectedOutput = String.format("%s%s%s%s%s",
                engine.getWorkItems().get(1), engine.getWorkItems().get(0), engine.getWorkItems().get(7),
                engine.getWorkItems().get(2), engine.getWorkItems().get(6));

        Assert.assertEquals(expectedOutput, new SortWorkItemByPriorityCommand(engine).execute(new ArrayList<>()));
    }

    @Test
    public void sortByPriorityCommand_Should_ReturnProperMessageWhenThereAreNoWorkItems() {
        String expectedOutput = "There are no workitems with this priority";

        Assert.assertEquals(expectedOutput, new SortWorkItemByPriorityCommand(engine).execute(new ArrayList<>()));
    }
}

package commands.sort.commands;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.SeverityType;
import enums.StorySizeType;
import implementations.IDGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class SortByRatingCommandTest {

    private final Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    private void createWorkItems() {
        engine.getWorkItems().add(factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.MAJOR));
        engine.getWorkItems().add(factory.createBug("titleTesting2", "descriptionTest2", new ArrayList<>(),
                PriorityType.MEDIUM, SeverityType.CRITICAL));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting3", "descriptionTest3", 2));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting4", "descriptionTest4", 5));
        engine.getWorkItems().add(factory.createFeedBack("titleTesting5", "descriptionTest5", 1));
        engine.getWorkItems().add(factory.createStory("titleTesting6", "descriptionTest6",
                PriorityType.LOW, StorySizeType.LARGE));
    }

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        engine.getWorkItems().clear();
    }

    @Test
    public void sortByRatingCommand_Should_SortFeedbackItemsProperly() {
        createWorkItems();
        String expectedOutput = String.format("%s%s%s",
                engine.getWorkItems().get(4), engine.getWorkItems().get(2), engine.getWorkItems().get(3));

        Assert.assertEquals(expectedOutput, new SortWorkItemRatingCommand(engine).execute(new ArrayList<>()));
    }

    @Test
    public void sortByRatingCommand_Should_ReturnProperMessageWhenThereAreNoFeedbackItems() {
        String expectedOutput = "There are no workitems with this rating";

        Assert.assertEquals(expectedOutput, new SortWorkItemRatingCommand(engine).execute(new ArrayList<>()));
    }
}

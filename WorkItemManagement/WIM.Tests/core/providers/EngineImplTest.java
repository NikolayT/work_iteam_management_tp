package core.providers;

import commands.Factory;
import commands.FactoryImpl;
import core.providers.contracts.Engine;
import exceptions.ObjectNotFoundException;
import org.junit.Assert;
import org.junit.Test;


public class EngineImplTest {
    private Factory factory = new FactoryImpl();
    private Engine engine = new EngineImpl(factory);

    @Test
    public void getBoardByName(){
        engine.getBoards().add(factory.createBoard("myBoard"));
        String expectedOutput = "myBoard";

        Assert.assertEquals(expectedOutput,engine.getBoardByName("myBoard").getName());
    }
    @Test(expected = ObjectNotFoundException.class)
    public void getBoardByNameWhenThereIsNoSuchBoard_Should_ThrowException(){
        engine.getBoards().add(factory.createBoard("myBoard"));

        engine.getBoardByName("invalidName").getName();
    }
}
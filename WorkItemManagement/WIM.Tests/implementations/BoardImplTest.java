package implementations;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Board;
import contracts.Feedback;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BoardImplTest {

    private final static String BOARD_NAME = "boardName";

    private final Factory factory = new FactoryImpl();

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
    }

    @Test(expected = IllegalArgumentException.class)
    public void boardWithInvalidShortName_Should_ThrowException() {
        String invalidName = "A";
        factory.createBoard(invalidName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void boardWithInvalidLongName_Should_ThrowException() {
        String invalidName = "longInvalidName";
        factory.createBoard(invalidName);
    }

    @Test
    public void boardWithValidName_Should_ReturnTheName() {
        Assert.assertEquals(BOARD_NAME, factory.createBoard(BOARD_NAME).getName());
    }

    @Test
    public void creatingWorkItemOnBoard_Should_ReturnProperHistoryMessage() {
        Board board = factory.createBoard(BOARD_NAME);
        board.createNewWorkItem(factory.createFeedBack("titleTesting", "descriptionTest", 3));

        String expectedOutput = "boardName activity history:\n Feedback added to the board\n";
        Assert.assertEquals(expectedOutput, board.showBoardActivity());
    }

    @Test
    public void boardWithNoActivityHistory_Should_ReturnProperMessage() {
        String expectedOutput = "boardName has no activity yet";

        Assert.assertEquals(expectedOutput, factory.createBoard(BOARD_NAME).showBoardActivity());
    }

    @Test
    public void boardWithNoWorkItems_Should_ReturnProperMessage() {
        String expectedOutput = "boardName has no workitems yet";

        Assert.assertEquals(expectedOutput, factory.createBoard(BOARD_NAME).getBoardInfo());
    }

    @Test
    public void boardWithWorkItem_Should_ReturnProperMessage() {
        Board board = factory.createBoard(BOARD_NAME);
        Feedback workItem = factory.createFeedBack("titleTesting", "descriptionTest", 3);
        board.createNewWorkItem(workItem);
        String expectedOutput = String.format("Board Name: boardName\n%s\n", workItem.toString());

        Assert.assertEquals(expectedOutput, board.getBoardInfo());
    }
}

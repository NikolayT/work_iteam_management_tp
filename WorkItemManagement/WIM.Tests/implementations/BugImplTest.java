package implementations;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Bug;
import enums.PriorityType;
import enums.SeverityType;
import enums.StatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class BugImplTest {

    private final Factory factory = new FactoryImpl();

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeBugWithInvalidStatus_Should_ThrowException() {
        StatusType invalidStatusType = StatusType.DONE;
        factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL).changeStatus(invalidStatusType);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBugWithInvalidSeverity_Should_ThrowException() {
        String invalidSeverity = "invalidSeverity";
        factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.fromString(invalidSeverity));
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeStoryWithInvalidSeverity_Should_ThrowException() {
        String invalidSeverity = "invalidSeverity";
        factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL)
                .changeSeverity(SeverityType.fromString(invalidSeverity));
    }

    @Test
    public void changeBugSeverity_Should_ReturnProperHistoryMessage() {
        SeverityType severityType = SeverityType.MAJOR;
        Bug workItem = factory.createBug("titleTesting", "descriptionTest", new ArrayList<>(),
                PriorityType.HIGH, SeverityType.CRITICAL);

        workItem.changeSeverity(severityType);
        String expectedOutput = String.format("Severity of Bug with ID: 0000000001 was changed to %s", severityType);

        Assert.assertEquals(expectedOutput, workItem.getHistory().get(0));
    }
}

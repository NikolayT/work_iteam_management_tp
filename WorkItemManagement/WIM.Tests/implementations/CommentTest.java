package implementations;

import contracts.Person;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommentTest {
@Test
    public void toString_Should_ReturnRightOutput(){
    String personName = "test1";
    Person person = new PersonImpl(personName);
    String content = "test content";
    Comment comment = new Comment(content,person);
    String expectedOutput = String.format("Author: %s\n Comment: %s",personName,content);

    Assert.assertEquals(expectedOutput,comment.toString());
    }
}
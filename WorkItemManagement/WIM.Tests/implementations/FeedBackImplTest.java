package implementations;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Feedback;
import enums.StatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FeedBackImplTest {

    private final Factory factory = new FactoryImpl();

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeFeedBackWithInvalidStatus_Should_ThrowException() {
        StatusType invalidStatusType = StatusType.FIXED;
        factory.createFeedBack("titleTesting", "descriptionTest", 3).changeStatus(invalidStatusType);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFeedBackWithInvalidRating_Should_ThrowException() {
        int invalidRating = -1;
        factory.createFeedBack("titleTesting", "descriptionTest", invalidRating);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeFeedBackWithInvalidRating_Should_ThrowException() {
        int invalidRating = -1;
        factory.createFeedBack("titleTesting", "descriptionTest", 5).changeRating(invalidRating);
    }

    @Test
    public void changeFeedBackRating_Should_ReturnProperHistoryMessage() {
        int rating = 5;
        String expectedOutput = String.format("Rating of Feedback with ID: 0000000001 was changed to %d", rating);
        Feedback workItem = factory.createFeedBack("titleTesting", "descriptionTest", 3);
        workItem.changeRating(rating);

        Assert.assertEquals(expectedOutput, workItem.getHistory().get(0));
    }
}

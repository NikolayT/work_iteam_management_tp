package implementations;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Person;
import contracts.Story;
import enums.PriorityType;
import enums.StorySizeType;
import exceptions.DublicatedNameException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class PersonImplTest {

    private final static String TEST_NAME = "Pesho";
    private final static String INVALID_TEST_NAME = "Ivan";

    private final Factory factory = new FactoryImpl();

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidNameLength_Should_ThrowException() {
        factory.createPerson(INVALID_TEST_NAME);
    }

    @Test(expected = DublicatedNameException.class)
    public void creatingPeopleWithIdenticalName_Should_ThrowException() {
        factory.createPerson(TEST_NAME);
        factory.createPerson(TEST_NAME);
    }

    @Test
    public void personWithNoActivity_Should_ReturnExpectedMessage() {
        Person person = factory.createPerson(TEST_NAME);
        String expectedOutput = person.getName() + " has no activity yet";

        Assert.assertEquals(expectedOutput, person.showPersonActivity());
    }

    @Test
    public void personWithActivity_Should_ReturnExpectedMessage() {
        Person person = factory.createPerson(TEST_NAME);
        String teamName = "MyTeam";
        factory.createTeam(teamName).addPerson(person);
        String expectedOutput = String.format("%s activity history:\n%s was added to team - %s\n", person.getName(), person.getName(), teamName);

        Assert.assertEquals(expectedOutput, person.showPersonActivity());
    }

    @Test
    public void assigningWorkItem_Should_ReturnRightMessage() {
        Story story = new StoryImpl("titletitle", "descriptiontest", PriorityType.HIGH, StorySizeType.LARGE);
        String expectedOutput = String.format("%s%s", "Person got an item from the board with title: ", story.getTitle());

        Assert.assertEquals(expectedOutput, factory.createPerson(TEST_NAME).assignWorkItem(story));
    }

    @Test
    public void unassigningWorkItem_Should_ReturnRightMessage() {
        Person person = factory.createPerson(TEST_NAME);
        Story story = new StoryImpl("titletitle", "descriptiontest", PriorityType.HIGH, StorySizeType.LARGE);
        person.assignWorkItem(story);
        String expectedOutput = String.format("%s%s", "Person set item on the board with title: ", story.getTitle());

        Assert.assertEquals(expectedOutput, person.unassignWorkItem(story));
    }

    @Test
    public void assingingSameWorkItem_Should_ReturnProperMessage() {
        Person person = factory.createPerson(TEST_NAME);
        Story story = new StoryImpl("titletitle", "descriptiontest", PriorityType.MEDIUM, StorySizeType.SMALL);
        person.assignWorkItem(story);
        String expectedOutput = String.format("WorkItem with ID: %s already assigned", story.getID());

        Assert.assertEquals(expectedOutput, person.assignWorkItem(story));
    }

    @Test
    public void unassigningWorkItemThatIsNotAssigned_Should_ReturnRightMessage() {
        Story story = new StoryImpl("titletitle", "descriptiontest", PriorityType.HIGH, StorySizeType.LARGE);
        String expectedOutput = "Pesho does not have this work item";

        Assert.assertEquals(expectedOutput, factory.createPerson(TEST_NAME).unassignWorkItem(story));
    }
}

package implementations;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Story;
import enums.PriorityType;
import enums.StatusType;
import enums.StorySizeType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StoryImplTest {

    private final Factory factory = new FactoryImpl();

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeStoryWithInvalidStatus_Should_ThrowException() {
        StatusType invalidStatusType = StatusType.FIXED;
        factory.createStory("titleTesting", "descriptionTest", PriorityType.HIGH, StorySizeType.MEDIUM)
                .changeStatus(invalidStatusType);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createStoryWithInvalidSizeType_Should_ThrowException() {
        String invalidSize = "invalidSize";
        factory.createStory("titleTesting", "descriptionTest", PriorityType.HIGH,
                StorySizeType.fromString(invalidSize));
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeStoryWithInvalidSizeType_Should_ThrowException() {
        String invalidSize = "invalidSize";
        factory.createStory("titleTesting", "descriptionTest", PriorityType.HIGH, StorySizeType.MEDIUM)
                .changeSize(StorySizeType.fromString(invalidSize));
    }

    @Test
    public void changeStorySizeType_Should_ReturnProperHistoryMessage() {
        StorySizeType sizeType = StorySizeType.MEDIUM;
        String expectedOutput = String.format("Size of Story with ID: 0000000001 was changed to %s", sizeType);
        Story workItem = factory.createStory("titleTesting", "descriptionTest", PriorityType.HIGH, StorySizeType.LARGE);
        workItem.changeSize(sizeType);

        Assert.assertEquals(expectedOutput, workItem.getHistory().get(0));
    }
}

package implementations;

import commands.Factory;
import commands.FactoryImpl;
import contracts.Person;
import contracts.Story;
import contracts.Team;
import enums.PriorityType;
import enums.StorySizeType;
import exceptions.DublicatedBoardNameException;
import exceptions.DublicatedNameException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TeamImplTest {

    private final static String TEAM_NAME = "teamName";
    private final static String PERSON_NAME = "personName";
    private final static String BOARD_NAME = "boardName";

    private final Factory factory = new FactoryImpl();

    @Before
    public void setUp() {
        IDGenerator.clearGenerator();
    }

    @After
    public void cleanUp() {
        PersonImpl.clearNames();
    }

    @Test(expected = IllegalArgumentException.class)
    public void addingInvalidPersonToTeam_Should_ThrowException() {
        String invalidPersonName = "thisNameIsTooLongToBeValid";
        factory.createTeam(TEAM_NAME).addPerson(factory.createPerson(invalidPersonName));
    }

    @Test(expected = DublicatedNameException.class)
    public void addingSamePersonNameToTeam_Should_ThrowException() {
        Team team = factory.createTeam(TEAM_NAME);
        team.addPerson(factory.createPerson(PERSON_NAME));
        team.addPerson(factory.createPerson(PERSON_NAME));
    }

    @Test
    public void addingPersonToTeam_Should_IncreaseMembersInTeam() {
        int expectedNumberOfMembers = 1;
        Team team = factory.createTeam(TEAM_NAME);
        team.addPerson(factory.createPerson(PERSON_NAME));

        Assert.assertEquals(expectedNumberOfMembers, team.getNumberOfTeamMembers());
    }

    @Test
    public void showTeamWhenNoMembersRegistered_Should_ReturnMessage() {
        String expectedOutput = "There are no members added yet.";

        Assert.assertEquals(expectedOutput, factory.createTeam(TEAM_NAME).showAllTeamMembers());
    }

    @Test
    public void showTeamWhenPersonAddedToTeam_Should_ReturnMessage() {
        Person person = factory.createPerson(PERSON_NAME);
        Team team = factory.createTeam(TEAM_NAME);
        team.addPerson(person);
        String expectedOutput = String.format("Team Members:\n %s\n", person.getName());

        Assert.assertEquals(expectedOutput, team.showAllTeamMembers());
    }

    @Test
    public void showingTeamBoardsWithNoneCreated_Should_ReturnProperMessage() {
        String expectedOutput = "There are no boards added yet.";

        Assert.assertEquals(expectedOutput, factory.createTeam(TEAM_NAME).showAllTeamBoards());
    }

    @Test
    public void showTeamBoardsWithEmptyBoard_Should_ReturnMessage() {
        Team team = factory.createTeam(TEAM_NAME);
        team.createNewBoard(BOARD_NAME);
        String expectedOutput = "Team Boards:\nboardName has no workitems yet";

        Assert.assertEquals(expectedOutput, team.showAllTeamBoards());
    }

    @Test(expected = DublicatedBoardNameException.class)
    public void creatingIdenticalTeamBoards_Should_ThrowException() {
        Team team = factory.createTeam(TEAM_NAME);
        team.createNewBoard(BOARD_NAME);
        team.createNewBoard(BOARD_NAME);
    }

    @Test
    public void creatingNewBoard_Should_IncreaseBoardSizeInTeam() {
        int expectedNumberOfBoards = 1;
        Team team = factory.createTeam(TEAM_NAME);
        team.createNewBoard(BOARD_NAME);

        Assert.assertEquals(expectedNumberOfBoards, team.getNumberOfTeamBoards());
    }

    @Test
    public void showAllTeamMembersWithNoMembers_Should_ReturnProperMessage() {
        String expectedOutput = "There are no members added yet.";

        Assert.assertEquals(expectedOutput, factory.createTeam(TEAM_NAME).showAllTeamMembers());
    }

    @Test
    public void showAllTeamMembersWithMember_Should_ReturnProperMessage() {
        Team team = factory.createTeam(TEAM_NAME);
        team.addPerson(factory.createPerson(PERSON_NAME));

        String expectedOutput = String.format("Team Members:\n %s\n", PERSON_NAME);

        Assert.assertEquals(expectedOutput, team.showAllTeamMembers());
    }

    @Test
    public void showAllTeamBoardsWithNoBoards_Should_ReturnProperMessage() {
        String expectedOutput = "There are no boards added yet.";

        Assert.assertEquals(expectedOutput, factory.createTeam(TEAM_NAME).showAllTeamBoards());
    }

    @Test
    public void showAllTeamBoardsWithWorkItemOnBoard_Should_ReturnProperMessage() {
        Team team = factory.createTeam(TEAM_NAME);
        team.createNewBoard(BOARD_NAME);
        Story story = factory.createStory("titletitle", "descriptiontest", PriorityType.HIGH, StorySizeType.LARGE);
        team.addWorkItemOnBoard(BOARD_NAME, story);
        String expectedOutput = String.format("Team Boards:\nBoard Name: %s\n%s\n", BOARD_NAME, story.toString());

        Assert.assertEquals(expectedOutput, team.showAllTeamBoards());
    }

    @Test
    public void showTeam_Should_ReturnProperMessage() {
        Team team = factory.createTeam(TEAM_NAME);
        team.createNewBoard(BOARD_NAME);
        Story story = factory.createStory("titletitle", "descriptiontest", PriorityType.HIGH, StorySizeType.LARGE);
        team.addWorkItemOnBoard(BOARD_NAME, story);
        team.addPerson(factory.createPerson(PERSON_NAME));

        String expectedOutput = String.format("%s\nTeam Members:\n %s\nTeam Boards:\nBoard Name: %s\n%s\n",
                TEAM_NAME, PERSON_NAME, BOARD_NAME, story.toString());
        Assert.assertEquals(expectedOutput, team.showTeam());
    }

    @Test
    public void showTeamActivityWhenThereIsNone_Should_ReturnExpectedMessage() {
        String expectedOutput = String.format("%s has no activity yet\n", TEAM_NAME);

        Assert.assertEquals(expectedOutput, factory.createTeam(TEAM_NAME).showTeamActivity());
    }

    @Test
    public void showTeamActivityWhenPersonAdded_Should_ReturnExpectedMessage() {
        Team team = factory.createTeam(TEAM_NAME);
        String personName = "test1";
        team.addPerson(factory.createPerson(personName));
        String expectedOutput = String.format("%s activity:\n%s activity history:\n%s was added to team - %s\n"
                , team.getName(), personName, personName, team.getName());

        Assert.assertEquals(expectedOutput, team.showTeamActivity());
    }

}
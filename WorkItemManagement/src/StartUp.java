import commands.Factory;
import commands.FactoryImpl;
import core.providers.EngineImpl;
import core.providers.contracts.Engine;

public class StartUp {
    public static void main(String[] args) {
        Factory factory = new FactoryImpl();
        Engine engine = new EngineImpl(factory);
        engine.start();
    }
}

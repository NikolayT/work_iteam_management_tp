package base;

import contracts.Person;
import contracts.SubBaseWorkItemInterface;
import enums.StatusType;
import enums.PriorityType;

public abstract class SubBaseWorkItem extends WorkItemBase implements SubBaseWorkItemInterface {

    private static final String CHANGING_PRIORITY = "Priority was changed to";

    private PriorityType priority;
    private Person assignee;

    public SubBaseWorkItem(String title, String description, PriorityType priority, StatusType statusType) {
        super(title, description, statusType);
        setPriority(priority);
    }

    public PriorityType getPriority() {
        return priority;
    }

    public Person getAssignee() {
        return assignee;
    }

    public void assign(Person assignee) {
        this.assignee = assignee;
    }

    public void unassign() {
        this.assignee = null;
    }

    public void changePriority(PriorityType priorityType) {
        setPriority(priorityType);

        super.addActivityHistory(String.format("%s %s of Story with id : %s",
                CHANGING_PRIORITY, priorityType.getTextValue(), getID()));
    }

    private void setPriority(PriorityType priority) {
        if (priority == null) {
            throw new IllegalArgumentException();
        }

        this.priority = priority;
    }
}

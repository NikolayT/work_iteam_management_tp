package base;

import contracts.WorkItem;
import implementations.Comment;
import implementations.IDGenerator;
import enums.StatusType;

import java.util.ArrayList;
import java.util.List;

public abstract class WorkItemBase implements WorkItem {

    private static final int MIN_TITLE_LENGTH = 10;
    private static final int MAX_TITLE_LENGTH = 50;

    private static final int MIN_DESCRIPTION_LENGTH = 10;
    private static final int MAX_DESCRIPTION_LENGTH = 500;

    private static final String CHANGING_STATUS = "Status was changed to ";

    private String id;
    private String title;
    private String description;
    private StatusType statusType;
    private List<String> history;
    private List<Comment> comments;

    public WorkItemBase(String title, String description, StatusType statusType) {
        id = IDGenerator.generateID();
        setTitle(title);
        setDescription(description);
        setStatusType(statusType);
        history = new ArrayList<>();
        comments = new ArrayList<>();
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public StatusType getStatusType() {
        return statusType;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    @Override
    public String toString() {
        return String.format("Type: %s\nID: %s\nTitle: %s\nDescription:\n" +
                        "%s\n%s\nStatus Type: %s\nComments:\n%sHistory:\n%s\n",
                getClassName(), id, title, description, additionalInfo(), statusType.getTextValue(), listComments(), listHistory());
    }

    protected abstract StatusType validateStatusType(StatusType statusType);

    protected abstract String additionalInfo();

    protected void addActivityHistory(String activity) {
        this.history.add(activity);
    }

    @Override
    public void changeStatus(StatusType statusType) {
        setStatusType(statusType);
        addActivityHistory(String.format("%s %s of %s with id: %s",
                CHANGING_STATUS, statusType.getTextValue(), getClassName(), id));
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    private void setStatusType(StatusType statusType) {
        this.statusType = validateStatusType(statusType);
    }

    private void setTitle(String title) {
        if (title.length() < MIN_TITLE_LENGTH || title.length() > MAX_TITLE_LENGTH) {
            throw new IllegalArgumentException(String.format(
                    "The title length should be between %d and %d symbols", MIN_TITLE_LENGTH, MAX_TITLE_LENGTH));
        }

        this.title = title;
    }

    private void setDescription(String description) {
        if (description.length() < MIN_DESCRIPTION_LENGTH || description.length() > MAX_DESCRIPTION_LENGTH) {
            throw new IllegalArgumentException(String.format(
                    "The description length should be between %d and %d symbols", MIN_DESCRIPTION_LENGTH, MAX_DESCRIPTION_LENGTH));
        }

        this.description = description;
    }

    private String listComments() {
        StringBuilder sb = new StringBuilder();
        for (Comment comment : comments) {
            sb.append(String.format("  %s\n", comment.toString()));
        }

        return sb.toString();
    }

    private String listHistory() {
        if (history.size()== 0) return "";
        StringBuilder sb = new StringBuilder();
        for (String activity : history) {
            sb.append(String.format("  %s\n", activity));
        }

        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }
}

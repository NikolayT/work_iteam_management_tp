package commands;

import contracts.*;
import enums.PriorityType;
import enums.SeverityType;
import enums.StorySizeType;
import implementations.*;

import java.util.List;

public interface Factory {
    Comment createComment(String content, Person person);

    Bug createBug(String title, String description, List<String> stepsToReproduce,
                  PriorityType priority, SeverityType severityType);

    Feedback createFeedBack(String title, String description, int rating);

    Person createPerson(String name);

    Story createStory(String title, String description, PriorityType priority, StorySizeType sizeType);

    Team createTeam(String name);

    Board createBoard(String name);

}

package commands;

import contracts.*;

import enums.PriorityType;
import enums.SeverityType;
import enums.StorySizeType;
import implementations.*;

import java.util.List;

public class FactoryImpl implements Factory {
    public Comment createComment(String content, Person person) {
        return new Comment(content, person);
    }

    public Bug createBug(String title, String description, List<String> stepsToReproduce,
                         PriorityType priority, SeverityType severityType) {
        return new BugImpl(title, description, stepsToReproduce, priority, severityType);
    }

    public Feedback createFeedBack(String title, String description, int rating) {
        return new FeedBackImpl(title, description, rating);
    }

    public Person createPerson(String name) {
        return new PersonImpl(name);
    }

    public Story createStory(String title, String description, PriorityType priority, StorySizeType sizeType) {
        return new StoryImpl(title, description, priority, sizeType);
    }

    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    public Board createBoard(String name) {
        return new BoardImpl(name);
    }
}

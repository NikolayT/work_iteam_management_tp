package commands.adding.commands;

import commands.Factory;
import commands.contracts.Command;
import core.providers.contracts.Engine;
import exceptions.ObjectNotFoundException;
import implementations.Comment;

import java.util.List;

public class AddCommentCommand implements Command {

    private final static int COMMAND_PARAMETERS = 3;

    private final Factory factory;
    private final Engine engine;

    public AddCommentCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String workItemID = parameters.get(0);
            String content = parameters.get(1);
            String personName = parameters.get(2);

            Comment comment = factory.createComment(content, engine.getPersonByName(personName));
            engine.getComments().add(comment);
            engine.getWorkItemByID(workItemID).addComment(comment);

            return String.format("%s added a comment for workitem with ID: %s", personName, workItemID);
        } catch (ObjectNotFoundException e) {
            return e.getMessage();
        }
    }
}

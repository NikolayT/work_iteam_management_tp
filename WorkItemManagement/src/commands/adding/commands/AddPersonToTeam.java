package commands.adding.commands;

import commands.contracts.Command;
import core.providers.contracts.Engine;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class AddPersonToTeam implements Command {

    private final static int COMMAND_PARAMETERS = 2;
    private final Engine engine;

    public AddPersonToTeam(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String personName = parameters.get(0);
            String teamName = parameters.get(1);

            engine.getTeamByName(teamName).addPerson(engine.getPersonByName(personName));
            return String.format("Person with name %s was added to team with name %s.", personName, teamName);
        } catch (ObjectNotFoundException e) {
            return e.getMessage();
        }
    }
}

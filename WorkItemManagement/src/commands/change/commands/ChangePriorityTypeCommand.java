package commands.change.commands;

import commands.contracts.Command;
import base.SubBaseWorkItem;
import core.providers.contracts.Engine;
import enums.PriorityType;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class ChangePriorityTypeCommand implements Command {

    private final static int COMMAND_PARAMETERS = 2;
    private static final String NO_PERMISION_FOR_DOING_THIS = "This workitem does not have permission to do that";

    private final Engine engine;

    public ChangePriorityTypeCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String id = parameters.get(0);
            String priorityType = parameters.get(1);

            if (engine.getWorkItemByID(id).getClassName().equals("Story") || engine.getWorkItemByID(id).getClassName().equals("Bug")) {
                ((SubBaseWorkItem) engine.getWorkItemByID(id)).changePriority(PriorityType.fromString(priorityType));
            } else {
                throw new IllegalArgumentException(NO_PERMISION_FOR_DOING_THIS);
            }
            
            return String.format("Priority type of workitem with id %s, was changed to %s", id, priorityType);

        } catch (ObjectNotFoundException e) {
            return e.getMessage();
        }
    }
}

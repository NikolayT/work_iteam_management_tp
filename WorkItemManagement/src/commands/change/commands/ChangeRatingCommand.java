package commands.change.commands;

import commands.contracts.Command;
import contracts.Feedback;
import core.providers.contracts.Engine;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class ChangeRatingCommand implements Command {

    private final static int COMMAND_PARAMETERS = 2;

    private final Engine engine;
    private static final String NO_PERMISION_FOR_DOING_THIS = "This workitem is not Feedback and cannot change its size, because does not have.";

    public ChangeRatingCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String id = parameters.get(0);
            int rating = Integer.parseInt(parameters.get(1));
            if (!engine.getWorkItemByID(id).getClassName().equals("Feedback")) {
                throw new IllegalArgumentException(NO_PERMISION_FOR_DOING_THIS);
            }
            ((Feedback) engine.getWorkItemByID(id)).changeRating(rating);
            return String.format("Rating of workitem with id %s, was changed to %s", id, rating);

        } catch (ObjectNotFoundException e) {
            return e.getMessage();
        }
    }
}

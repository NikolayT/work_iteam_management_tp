package commands.change.commands;

import commands.contracts.Command;
import contracts.Bug;
import core.providers.contracts.Engine;
import enums.SeverityType;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class ChangeSeverityCommand implements Command {

    private final static int COMMAND_PARAMETERS = 2;
    private static final String NO_PERMISION_FOR_DOING_THIS = "This workitem is not Bug and cannot change its severity type";

    private final Engine engine;

    public ChangeSeverityCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String id = parameters.get(0);
            String severityType = parameters.get(1);

            if (!engine.getWorkItemByID(id).getClassName().equals("Bug")) {
                throw new IllegalArgumentException(NO_PERMISION_FOR_DOING_THIS);
            }
            if (!engine.getWorkItemByID(id).getClassName().equals("Bug")) {
                throw new IllegalArgumentException(NO_PERMISION_FOR_DOING_THIS);
            }

            ((Bug) engine.getWorkItemByID(id)).changeSeverity(SeverityType.fromString(severityType));
            return String.format("Severity type of workitem with id %s, was changed to %s", id, severityType);

        } catch (ObjectNotFoundException e) {
            return e.getMessage();
        }
    }
}


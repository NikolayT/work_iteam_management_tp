package commands.change.commands;

import commands.contracts.Command;
import contracts.Story;
import core.providers.contracts.Engine;
import enums.StorySizeType;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class ChangeSizeCommand implements Command {

    private final static int COMMAND_PARAMETERS = 2;
    private static final String NO_PERMISION_FOR_DOING_THIS = "This workitem is not Story and cannot change its size, because does not have.";

    private final Engine engine;

    public ChangeSizeCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String id = parameters.get(0);
            String sizeType = parameters.get(1);

            if (!engine.getWorkItemByID(id).getClassName().equals("Story")) {
                throw new IllegalArgumentException(NO_PERMISION_FOR_DOING_THIS);
            }

            ((Story) engine.getWorkItemByID(id)).changeSize(StorySizeType.fromString(sizeType));
            return String.format("Size of workitem with id %s, was changed to %s", id, sizeType);

        } catch (ObjectNotFoundException e) {
            return e.getMessage();
        }
    }

}

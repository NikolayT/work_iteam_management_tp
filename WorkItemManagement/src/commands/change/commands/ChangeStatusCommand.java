package commands.change.commands;

import commands.contracts.Command;
import core.providers.contracts.Engine;
import enums.StatusType;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class ChangeStatusCommand implements Command {

    private final static int COMMAND_PARAMETERS = 2;
    private final Engine engine;

    public ChangeStatusCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String id = parameters.get(0);
            String statusType = parameters.get(1);
            engine.getWorkItemByID(id).changeStatus(StatusType.fromString(statusType));
            return String.format("Status type of workitem with id %s, was changed to %s", id, statusType);

        } catch (ObjectNotFoundException e) {
            return e.getMessage();
        }
    }

}

package commands.contracts;

import java.util.List;

public interface Command {
    String FAILED_TO_PARSE_COMMAND_PARAMETERS = "Failed to parse %s parameters";

    String execute(List<String> parameters);
}


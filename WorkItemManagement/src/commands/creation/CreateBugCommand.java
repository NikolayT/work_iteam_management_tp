package commands.creation;

import commands.Factory;
import commands.contracts.Command;
import contracts.Bug;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.SeverityType;
import exceptions.ObjectNotFoundException;

import java.util.Arrays;
import java.util.List;

public class CreateBugCommand implements Command {

    private final static int COMMAND_PARAMETERS = 7;

    private final Factory factory;
    private final Engine engine;

    public CreateBugCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String teamName = parameters.get(0);
            String boardName = parameters.get(1);
            String title = parameters.get(2);
            String description = parameters.get(3);
            String priority = parameters.get(4);
            String severityType = parameters.get(5);
            String stepsInString = parameters.get(6);
            List<String> stepsToReproduce = Arrays.asList(stepsInString.split(","));
            Bug bug = factory.createBug(title, description, stepsToReproduce, PriorityType.fromString(priority),
                    SeverityType.fromString(severityType));

            engine.getTeamByName(teamName).addWorkItemOnBoard(boardName, bug);
            engine.getWorkItems().add(bug);
            engine.getBoardByName(boardName).createNewWorkItem(bug);
            return String.format("Bug with ID %s was created.", bug.getID());
        } catch (ObjectNotFoundException | IllegalArgumentException e) {
            return e.getMessage();
        }
    }
}

package commands.creation;

import commands.Factory;
import commands.contracts.Command;
import contracts.Board;
import core.providers.contracts.Engine;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class CreateNewBoardInATeamCommand implements Command {

    private final static int COMMAND_PARAMETERS = 2;

    private final Factory factory;
    private final Engine engine;

    public CreateNewBoardInATeamCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String name = parameters.get(0);
            String nameOfTeam = parameters.get(1);

            Board board = factory.createBoard(name);
            engine.getBoards().add(board);
            engine.getTeamByName(nameOfTeam).createNewBoard(name);

            return String.format("Board with name %s was created.", name);
        } catch (ObjectNotFoundException | IllegalArgumentException e) {
            return e.getMessage();
        }
    }
}

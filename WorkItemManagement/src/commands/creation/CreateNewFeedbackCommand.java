package commands.creation;

import commands.Factory;
import commands.contracts.Command;
import contracts.Feedback;
import core.providers.contracts.Engine;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class CreateNewFeedbackCommand implements Command {

    private final static int COMMAND_PARAMETERS = 5;

    private final Factory factory;
    private final Engine engine;

    public CreateNewFeedbackCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String teamName = parameters.get(0);
            String boardName = parameters.get(1);
            String title = parameters.get(2);
            String description = parameters.get(3);
            int rating = Integer.parseInt(parameters.get(4));

            Feedback feedback = factory.createFeedBack(title, description,rating);
            engine.getTeamByName(teamName).addWorkItemOnBoard(boardName, feedback);
            engine.getWorkItems().add(feedback);
            engine.getBoardByName(boardName).createNewWorkItem(feedback);

            return String.format("Feedback with ID %s was created.", feedback.getID());
        } catch (ObjectNotFoundException | IllegalArgumentException e) {
            return e.getMessage();
        }
    }
}



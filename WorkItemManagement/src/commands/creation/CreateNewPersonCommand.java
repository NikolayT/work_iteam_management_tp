package commands.creation;

import commands.Factory;
import commands.contracts.Command;
import contracts.Person;
import core.providers.contracts.Engine;
import exceptions.DublicatedNameException;

import java.util.List;

public class CreateNewPersonCommand implements Command {

    private final static int COMMAND_PARAMETERS = 1;

    private final Factory factory;
    private final Engine engine;

    public CreateNewPersonCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String personName = parameters.get(0);
            Person person = factory.createPerson(personName);
            engine.getPeople().add(person);
            return String.format("Person with name %s was created.", personName);
        } catch (IllegalArgumentException | DublicatedNameException e) {
            return e.getMessage();
        }
    }
}

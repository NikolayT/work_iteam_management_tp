package commands.creation;

import commands.Factory;
import commands.contracts.Command;
import contracts.Story;
import core.providers.contracts.Engine;
import enums.PriorityType;
import enums.StorySizeType;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class CreateNewStoryCommand implements Command {

    private final static int COMMAND_PARAMETERS = 6;

    private final Factory factory;
    private final Engine engine;

    public CreateNewStoryCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String teamName = parameters.get(0);
            String boardName = parameters.get(1);
            String title = parameters.get(2);
            String description = parameters.get(3);
            String priority = parameters.get(4);
            String size = parameters.get(5);

            Story story = factory.createStory(title, description, PriorityType.fromString(priority), StorySizeType.fromString(size));
            engine.getTeamByName(teamName).addWorkItemOnBoard(boardName, story);
            engine.getWorkItems().add(story);
            engine.getBoardByName(boardName).createNewWorkItem(story);

            return String.format("Story with ID %s was created.", story.getID());
        } catch (ObjectNotFoundException | IllegalArgumentException e) {
            return e.getMessage();
        }
    }


}

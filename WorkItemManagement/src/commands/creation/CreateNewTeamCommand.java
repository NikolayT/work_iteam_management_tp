package commands.creation;

import commands.Factory;
import commands.contracts.Command;
import contracts.Team;
import core.providers.contracts.Engine;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class CreateNewTeamCommand implements Command {

    private final static int COMMAND_PARAMETERS = 1;

    private final Factory factory;
    private final Engine engine;

    public CreateNewTeamCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String name = parameters.get(0);
            Team team = factory.createTeam(name);
            engine.getTeams().add(team);

            return String.format("Team with name %s was created.", name);
        } catch (ObjectNotFoundException e) {
            return e.getMessage();
        }
    }
}


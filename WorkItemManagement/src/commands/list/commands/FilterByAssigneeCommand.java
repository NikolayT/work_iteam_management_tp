package commands.list.commands;

import commands.contracts.Command;
import contracts.SubBaseWorkItemInterface;
import core.providers.contracts.Engine;

import java.util.List;

public class FilterByAssigneeCommand implements Command {

    private final static int COMMAND_PARAMETERS = 1;
    private static final String NO_WORKITEMS_WITH_THIS_ASSIGNEE = "There are no workitems with this assignee";
    private static final String UNASSIGNABLE_WORKITEM = "Feedback";

    private final Engine engine;

    public FilterByAssigneeCommand(Engine engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        String personName = parameters.get(0);
        StringBuilder workItemsList = new StringBuilder();
        engine.getWorkItems().stream()
                .filter(workItem -> !workItem.getClassName().equals(UNASSIGNABLE_WORKITEM))
                .filter(workItem -> (((SubBaseWorkItemInterface) workItem).getAssignee() != null) &&
                        ((SubBaseWorkItemInterface) workItem).getAssignee().getName().equalsIgnoreCase(personName))
                .forEach(workItem -> workItemsList.append(workItem.toString()));

        if (workItemsList.length() == 0) {
            return NO_WORKITEMS_WITH_THIS_ASSIGNEE;
        }

        return workItemsList.toString();
    }
}

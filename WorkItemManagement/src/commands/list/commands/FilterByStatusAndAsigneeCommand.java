package commands.list.commands;

import commands.contracts.Command;
import contracts.SubBaseWorkItemInterface;
import core.providers.contracts.Engine;

import java.util.List;

public class FilterByStatusAndAsigneeCommand implements Command {

    private final static int COMMAND_PARAMETERS = 2;
    private static final String UNASSIGNABLE_WORKITEM = "Feedback";
    private static final String NO_WORKITEMS_FOUND = "No workitems with this specification found";

    private final Engine engine;

    public FilterByStatusAndAsigneeCommand(Engine engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        String status = parameters.get(0);
        String personName = parameters.get(1);
        StringBuilder workItemsList = new StringBuilder();

        engine.getWorkItems().stream()
                .filter(workItem -> !(workItem.getClassName().equals(UNASSIGNABLE_WORKITEM)))
                .filter(workItem -> ((SubBaseWorkItemInterface) workItem).getAssignee() != null &&
                        ((SubBaseWorkItemInterface) workItem).getAssignee().getName().equalsIgnoreCase(personName)
                        && workItem.getStatusType().getTextValue().equalsIgnoreCase(status))
                .forEach(workItem -> workItemsList.append(workItem.toString()));

        if (workItemsList.length() == 0) {
            return NO_WORKITEMS_FOUND;
        }

        return workItemsList.toString();
    }
}

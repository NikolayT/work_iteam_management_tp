package commands.list.commands;

import commands.contracts.Command;
import core.providers.contracts.Engine;

import java.util.List;

public class FilterByStatusListCommand implements Command {

    private final static int COMMAND_PARAMETERS = 1;
    private static final String NO_WORKITEMS_WITH_THIS_STATUS = "There are no workitems with this status";

    private final Engine engine;

    public FilterByStatusListCommand(Engine engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        String status = parameters.get(0);
        StringBuilder workItemsList = new StringBuilder();

        engine.getWorkItems()
                .stream()
                .filter(workItem -> workItem.getStatusType().getTextValue().equalsIgnoreCase(status))
                .forEach(workItem -> workItemsList.append(workItem.toString()));

        if (workItemsList.length() == 0) {
            return NO_WORKITEMS_WITH_THIS_STATUS;
        }

        return workItemsList.toString();
    }

}

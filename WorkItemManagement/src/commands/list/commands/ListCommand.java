package commands.list.commands;

import commands.contracts.Command;
import core.providers.contracts.Engine;

import java.util.List;

public class ListCommand implements Command {

    private final static int COMMAND_PARAMETERS = 1;
    private static final String NO_WORKITEMS = "There are no workitems";
    private static final String NO_WORKITEMS_OF_THIS_TYPE = "There are no workitems of this type";

    private final Engine engine;

    public ListCommand(Engine engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        StringBuilder workItemsList = new StringBuilder();
        String typeOfListing = parameters.get(0);

        if (typeOfListing.equalsIgnoreCase("all")) {
            engine.getWorkItems()
                    .forEach(workItem -> workItemsList.append(workItem.toString()));
        } else {
            engine.getWorkItems()
                    .stream()
                    .filter(workItem -> workItem.getClassName().equalsIgnoreCase(typeOfListing))
                    .forEach(workItem -> workItemsList.append(workItem.toString()));
        }

        if (workItemsList.length() == 0) {
            if (typeOfListing.equalsIgnoreCase("all")) {
                return NO_WORKITEMS;
            }
            return NO_WORKITEMS_OF_THIS_TYPE;
        }

        return workItemsList.toString();
    }
}

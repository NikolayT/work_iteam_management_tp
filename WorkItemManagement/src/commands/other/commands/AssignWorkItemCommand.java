package commands.other.commands;

import commands.contracts.Command;
import contracts.Board;
import contracts.Person;
import contracts.Team;
import contracts.WorkItem;
import core.providers.contracts.Engine;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class AssignWorkItemCommand implements Command {

    private final static int COMMAND_PARAMETERS = 2;
    private static final String NO_WORKITEMS = "There are no workitems created";
    private static final String NO_PEOPLE = "There are no registered people";
    private static final String WORKITEM_NOT_ON_BOARD_OF_SAME_TEAM_WITH_PERSON = "This workitem is not on board from the same team as person";
    private static final String NOT_ASSIGNABLE_WORKITEM = "Workitem of type Feedback cannot have an assigner";

    private final Engine engine;
    private boolean assign;

    public AssignWorkItemCommand(Engine engine, boolean assign) {
        this.engine = engine;
        this.assign = assign;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        if (engine.getPeople().size() == 0) {
            return NO_PEOPLE;
        }

        if (engine.getWorkItems().size() == 0) {
            return NO_WORKITEMS;
        }

        String workItemID = parameters.get(0);
        String personName = parameters.get(1);

        try {
            if (engine.getWorkItemByID(workItemID).getClassName().equals("Feedback")) {
                return NOT_ASSIGNABLE_WORKITEM;
            }

            if (assign) {
                if (checkIfWorkItemIsAtBoardOfTheSameTeamAsPerson(engine.getPersonByName(personName), engine.getWorkItemByID(workItemID))) {
                    return engine.getPersonByName(personName).assignWorkItem(engine.getWorkItemByID(workItemID));
                } else {
                    return WORKITEM_NOT_ON_BOARD_OF_SAME_TEAM_WITH_PERSON;
                }
            } else {
                return engine.getPersonByName(personName).unassignWorkItem(engine.getWorkItemByID(workItemID));
            }
        } catch (ObjectNotFoundException e) {
            return e.getMessage();
        }
    }

    private boolean checkIfWorkItemIsAtBoardOfTheSameTeamAsPerson(Person person, WorkItem workItem) {
        for (Team team : engine.getTeams()) {
            if (team.getMembers().contains(person)) {
                for (Board board : team.getBoards()) {
                    if (board.getWorkItems().contains(workItem)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

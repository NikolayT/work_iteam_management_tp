package commands.show.commands;

import commands.contracts.Command;
import contracts.Person;
import core.providers.contracts.Engine;

import java.util.List;

public class ShowAllPeopleCommand implements Command {
    private static final String NO_PEOPLE_MESSAGE = "There are no registered people.";

    private final Engine engine;

    public ShowAllPeopleCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        List<Person> people = engine.getPeople();

        if (people.size() == 0) {
            return NO_PEOPLE_MESSAGE;
        }

        StringBuilder allPeople = new StringBuilder();
        for (Person p : people) {
            allPeople.append(String.format("Person: %s\n", p.getName()));
        }
        return allPeople.toString();
    }
}

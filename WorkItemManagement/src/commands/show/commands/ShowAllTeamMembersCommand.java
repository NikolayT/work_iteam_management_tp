package commands.show.commands;

import commands.contracts.Command;
import core.providers.contracts.Engine;
import exceptions.ObjectNotFoundException;

import java.util.List;

public class ShowAllTeamMembersCommand implements Command {

    private final static int COMMAND_PARAMETERS = 1;

    private final Engine engine;

    public ShowAllTeamMembersCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != COMMAND_PARAMETERS) {
            return String.format(FAILED_TO_PARSE_COMMAND_PARAMETERS, this.getClass().getSimpleName());
        }

        try {
            String teamName = parameters.get(0);
            return engine.getTeamByName(teamName).showAllTeamMembers();
        } catch (ObjectNotFoundException e) {
            return e.getMessage();
        }
    }
}

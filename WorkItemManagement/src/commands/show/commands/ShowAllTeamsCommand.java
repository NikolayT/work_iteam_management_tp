package commands.show.commands;

import commands.contracts.Command;
import contracts.Team;
import core.providers.contracts.Engine;

import java.util.List;

public class ShowAllTeamsCommand implements Command {

    private static final String NO_TEAMS_MESSAGE = "There are no registered teams.";

    private final Engine engine;

    public ShowAllTeamsCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        List<Team> teams = engine.getTeams();

        if (teams.size() == 0) {
            return NO_TEAMS_MESSAGE;
        }

        StringBuilder allTeams = new StringBuilder();
        for (Team t : teams) {
            allTeams.append(t.showTeam());
        }

        return allTeams.toString();
    }
}

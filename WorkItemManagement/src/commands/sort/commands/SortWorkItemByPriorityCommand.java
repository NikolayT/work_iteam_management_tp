package commands.sort.commands;

import commands.contracts.Command;
import base.SubBaseWorkItem;
import core.providers.contracts.Engine;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortWorkItemByPriorityCommand implements Command {

    private static final String NO_WORKITEMS_WITH_THIS_PRIORITY = "There are no workitems with this priority";

    private final Engine engine;

    public SortWorkItemByPriorityCommand(Engine engine) {
        this.engine = engine;
    }

    public String execute(List<String> parameters) {
        StringBuilder sortedItemsString = new StringBuilder();


        engine.getWorkItems().stream()
                .filter(workItem -> !workItem.getClassName().equals("Feedback"))
                .map(workItem -> (SubBaseWorkItem) workItem)
                .sorted(Comparator.comparing((SubBaseWorkItem::getPriority)))
                .collect(Collectors.toList())
                .forEach(item -> sortedItemsString.append(item.toString()));


        if (sortedItemsString.length() == 0) {
            return NO_WORKITEMS_WITH_THIS_PRIORITY;
        }
        return sortedItemsString.toString();
    }
}

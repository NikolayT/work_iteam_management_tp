package commands.sort.commands;

import commands.contracts.Command;
import contracts.Bug;
import core.providers.contracts.Engine;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortWorkItemBySeverityCommand implements Command {

    private static final String NO_WORKITEMS_WITH_THIS_SEVERITY = "There are no workitems with this severity";


    private final Engine engine;

    public SortWorkItemBySeverityCommand(Engine engine) {
        this.engine = engine;
    }
    public String execute(List<String> parameters) {
        StringBuilder sortedItemsString = new StringBuilder();

        engine.getWorkItems().stream()
                .filter(workItem -> workItem.getClassName().equals("Bug"))
                .map(workItem -> (Bug)workItem)
                .sorted(Comparator.comparing(Bug::getSeverityType))
                .collect(Collectors.toList())
                .forEach(item -> sortedItemsString.append(item.toString()));

        if (sortedItemsString.length() == 0) {
            return NO_WORKITEMS_WITH_THIS_SEVERITY;
        }
        return sortedItemsString.toString();
    }
}

package commands.sort.commands;

import commands.contracts.Command;
import contracts.Feedback;
import core.providers.contracts.Engine;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortWorkItemRatingCommand implements Command {

    private static final String NO_WORKITEMS_WITH_THIS_RATING = "There are no workitems with this rating";

    private final Engine engine;

    public SortWorkItemRatingCommand(Engine engine) {
        this.engine = engine;
    }
    public String execute(List<String> parameters) {
        StringBuilder sortedItemsString = new StringBuilder();

        engine.getWorkItems().stream()
                .filter(workItem -> workItem.getClassName().equals("Feedback"))
                .map(workItem -> (Feedback)workItem)
                .sorted(Comparator.comparing(Feedback::getRating))
                .collect(Collectors.toList())
                .forEach(item -> sortedItemsString.append(item.toString()));

        if (sortedItemsString.length() == 0) {
            return NO_WORKITEMS_WITH_THIS_RATING;
        }
        return sortedItemsString.toString();
    }
}

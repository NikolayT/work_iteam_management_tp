package commands.sort.commands;

import commands.contracts.Command;
import contracts.Story;
import core.providers.contracts.Engine;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortWorkItemSizeCommand implements Command {

    private static final String NO_WORKITEMS_WITH_THIS_SIZE = "There are no workitems with this size";

    private final Engine engine;

    public SortWorkItemSizeCommand(Engine engine) {
        this.engine = engine;
    }
    public String execute(List<String> parameters) {
        StringBuilder sortedItemsString = new StringBuilder();

        engine.getWorkItems().stream()
                .filter(workItem -> workItem.getClassName().equals("Story"))
                .map(workItem -> (Story)workItem)
                .sorted(Comparator.comparing(Story::getSize))
                .collect(Collectors.toList())
                .forEach(item -> sortedItemsString.append(item.toString()));

        if (sortedItemsString.length() == 0) {
            return NO_WORKITEMS_WITH_THIS_SIZE;
        }
        return sortedItemsString.toString();
    }
}

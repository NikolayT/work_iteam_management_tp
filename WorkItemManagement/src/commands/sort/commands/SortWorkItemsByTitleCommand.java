package commands.sort.commands;

import commands.contracts.Command;
import contracts.WorkItem;
import core.providers.contracts.Engine;

import java.util.Comparator;
import java.util.List;

public class SortWorkItemsByTitleCommand implements Command {

    private static final String NO_WORKITEMS_WITH_THIS_TITLE = "There are no workitems with this title";

    private final Engine engine;

    public SortWorkItemsByTitleCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        StringBuilder sortedItemsString = new StringBuilder();
        engine.getWorkItems()
                .stream()
                .sorted(Comparator.comparing(WorkItem::getTitle))
                .forEach(item -> sortedItemsString.append(item.toString()));
        if (sortedItemsString.length() == 0) {
            return NO_WORKITEMS_WITH_THIS_TITLE;
        }
        return sortedItemsString.toString();
    }
}
package contracts;

import java.util.List;

public interface Board {

    String getName();

    void createNewWorkItem(WorkItem workItem);

    String getBoardInfo();

    String showBoardActivity();

    List<WorkItem> getWorkItems();
}

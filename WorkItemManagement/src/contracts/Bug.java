package contracts;

import enums.SeverityType;

import java.util.List;

public interface Bug extends SubBaseWorkItemInterface {
    void changeSeverity(SeverityType severityType);

    SeverityType getSeverityType();

    List<String> getStepsToReproduce();
}

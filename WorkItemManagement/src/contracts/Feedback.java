package contracts;

public interface Feedback extends WorkItem {
    int getRating();

    void changeRating(int rating);
}

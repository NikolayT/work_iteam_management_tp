package contracts;

public interface Person {
    String assignWorkItem(WorkItem workItem);

    String unassignWorkItem(WorkItem workItem);

    String getName();

    String showPersonActivity();

    void addingActivityHistory(String command);
}

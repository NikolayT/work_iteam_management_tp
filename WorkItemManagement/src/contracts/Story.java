package contracts;

import enums.StorySizeType;

public interface Story extends SubBaseWorkItemInterface {
    StorySizeType getSize();

    void changeSize(StorySizeType storySizeType);
}

package contracts;

import enums.PriorityType;

public interface SubBaseWorkItemInterface extends WorkItem {

    PriorityType getPriority();

    Person getAssignee();

    void assign(Person assignee);

    void unassign();

    void changePriority(PriorityType priorityType);
}

package contracts;

import java.util.List;

public interface Team {
    void addPerson(Person person);

    void createNewBoard(String boardName);

    void addWorkItemOnBoard(String boardName, WorkItem workItem);

    String showAllTeamMembers();

    String showTeamActivity();

    String showAllTeamBoards();

    String showTeam();

    String getName();

    int getNumberOfTeamMembers();

    int getNumberOfTeamBoards();
    List<Person> getMembers();

    List<Board> getBoards();
}

package contracts;

import implementations.Comment;
import enums.StatusType;

import java.util.List;

public interface WorkItem {
    String getID();

    String getTitle();

    String toString();

    String getDescription();

    StatusType getStatusType();

    List<Comment> getComments();

    List<String> getHistory();

    void addComment(Comment comment);

    void changeStatus(StatusType statusType);

    String getClassName();
}

package core.providers;

import commands.Factory;
import commands.contracts.Command;
import contracts.Board;
import contracts.Person;
import contracts.Team;
import contracts.WorkItem;
import core.providers.providers.CommandParser;
import core.providers.providers.ConsoleReader;
import core.providers.providers.ConsoleWriter;
import core.providers.contracts.Engine;
import core.providers.contracts.Parser;
import core.providers.contracts.Reader;
import core.providers.contracts.Writer;
import exceptions.ObjectNotFoundException;
import implementations.Comment;

import java.util.ArrayList;
import java.util.List;

public class EngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";
    private static final String WORK_ITEM_NOT_FOUND = "There is no workitem with ID: %s";
    private static final String PERSON_NOT_FOUND = "Person with name %s does not exist";
    private static final String TEAM_NOT_FOUND = "Team with name %s does not exist";
    private static final String BOARD_NOT_FOUND = "Board with name %s does not belong to any board";

    private Reader reader;
    private Writer writer;
    private Parser parser;
    private final List<WorkItem> workItems;
    private final List<Person> people;
    private final List<Team> teams;
    private final List<Comment> comments;
    private final List<Board> boards;

    public EngineImpl(Factory factory) {
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        parser = new CommandParser(factory, this);

        workItems = new ArrayList<>();
        people = new ArrayList<>();
        teams = new ArrayList<>();

        comments = new ArrayList<>();

        boards = new ArrayList<>();
    }

    @Override
    public List<WorkItem> getWorkItems() {
        return workItems;
    }

    @Override
    public List<Person> getPeople() {
        return people;
    }

    @Override
    public List<Team> getTeams() {
        return teams;
    }

    @Override
    public List<Comment> getComments() {
        return comments;
    }

    @Override
    public List<Board> getBoards() {
        return boards;
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
            } catch (Exception ex) {
                writer.writeLine("Invalid arguments for the command");
            }
        }
    }

    @Override
    public WorkItem getWorkItemByID(String id) {
        for (WorkItem workItem : workItems) {
            if (workItem.getID().equals(id)) {
                return workItem;
            }
        }

        throw new ObjectNotFoundException(String.format(WORK_ITEM_NOT_FOUND, id));
    }

    @Override
    public Person getPersonByName(String name) {
        for (Person person : people) {
            if (person.getName().equals(name)) {
                return person;
            }
        }
        throw new ObjectNotFoundException(String.format(PERSON_NOT_FOUND, name));
    }

    @Override
    public Team getTeamByName(String name) {
        for (Team team : teams) {
            if (team.getName().equals(name)) {
                return team;
            }
        }
        throw new ObjectNotFoundException(String.format(TEAM_NOT_FOUND, name));
    }

    @Override
    public Board getBoardByName(String name) {
        for (Board board : boards) {
            if (board.getName().equals(name)) {
                return board;
            }
        }
        throw new ObjectNotFoundException(String.format(BOARD_NOT_FOUND, name));
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }
        Command command = parser.parseCommand(commandAsString);
        List<String> parameters = parser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}

package core.providers.contracts;

import contracts.Board;
import contracts.Person;
import contracts.Team;
import contracts.WorkItem;
import implementations.Comment;

import java.util.List;

public interface Engine {
    void start();

    List<WorkItem> getWorkItems();

    List<Person> getPeople();

    List<Team> getTeams();

    List<Comment> getComments();

    List<Board> getBoards();

    WorkItem getWorkItemByID(String id);

    Person getPersonByName(String name);

    Team getTeamByName(String name);

    Board getBoardByName(String name);
}

package core.providers.contracts;


public interface Reader {
    String readLine();
}

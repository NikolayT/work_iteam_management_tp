package core.providers.providers;

import commands.adding.commands.AddCommentCommand;
import commands.adding.commands.AddPersonToTeam;
import commands.change.commands.*;
import commands.show.commands.*;
import commands.creation.*;
import commands.Factory;
import commands.list.commands.FilterByAssigneeCommand;
import commands.list.commands.FilterByStatusAndAsigneeCommand;
import commands.list.commands.FilterByStatusListCommand;
import commands.list.commands.ListCommand;
import commands.other.commands.AssignWorkItemCommand;
import commands.sort.commands.*;
import commands.contracts.Command;
import core.providers.contracts.Engine;
import core.providers.contracts.Parser;

import java.util.ArrayList;
import java.util.List;

public class CommandParser implements Parser {
    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    private final Factory factory;
    private final Engine engine;

    public CommandParser(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public Command parseCommand(String fullCommand) {
        String commandName = fullCommand.split(" ")[0];
        return findCommand(commandName);
    }

    @Override
    public List<String> parseParameters(String fullCommand) {
        String[] commandParts = fullCommand.split(" ");
        ArrayList<String> parameters = new ArrayList<>();
        for (int i = 1; i < commandParts.length; i++) {
            parameters.add(commandParts[i]);
        }
        return parameters;
    }

    private Command findCommand(String commandName) {
        switch (commandName.toLowerCase()) {
            //creation commands
            case "createperson":
                return new CreateNewPersonCommand(factory, engine);
            case "createbug":
                return new CreateBugCommand(factory, engine);
            case "createnewboard":
                return new CreateNewBoardInATeamCommand(factory, engine);

            case "createfeedback":
                return new CreateNewFeedbackCommand(factory, engine);
            case "createstory":
                return new CreateNewStoryCommand(factory, engine);
            case "createteam":
                return new CreateNewTeamCommand(factory, engine);

            //adding commands
            case "addcomment":
                return new AddCommentCommand(factory, engine);
            case "addpersontoteam":
                return new AddPersonToTeam(engine);

            //show commands
            case "showallpeople":
                return new ShowAllPeopleCommand(engine);
            case "showallteams":
                return new ShowAllTeamsCommand(engine);
            case "showallteamboards":
                return new ShowAllTeamBoardsCommand(engine);
            case "showallteammembers":
                return new ShowAllTeamMembersCommand(engine);
            case "showpersonactivity":
                return new ShowPersonActivityCommand(engine);
            case "showboardactivity":
                return new ShowBoardsActivityCommand(engine);
            case "showteamactivity":
                return new ShowTeamActivityCommand(engine);

            //assign commands
            case "assignworkitem":
                return new AssignWorkItemCommand(engine, true);
            case "unassignworkitem":
                return new AssignWorkItemCommand(engine, false);

            // change commands
            case "changepriority":
                return new ChangePriorityTypeCommand(engine);
            case "changeseverity":
                return new ChangeSeverityCommand(engine);
            case "changesize":
                return new ChangeSizeCommand(engine);
            case "changestatus":
                return new ChangeStatusCommand(engine);
            case "changerating":
                return new ChangeRatingCommand(engine);

            // list commands
            case "listworkitems":
                return new ListCommand(engine);

            //list filtered
            case "listfilterbystatus":
                return new FilterByStatusListCommand(engine);
            case "listfilerbyassignee":
                return new FilterByAssigneeCommand(engine);
            case "listbystatusandassignee":
                return new FilterByStatusAndAsigneeCommand(engine);

            // sort commands
            case "sortworkitemsbytitle":
                return new SortWorkItemsByTitleCommand(engine);
            case "sortworkitembypriority":
                return new SortWorkItemByPriorityCommand(engine);
            case "sortworkitemsbyseverity":
                return new SortWorkItemBySeverityCommand(engine);
            case "sortworkitemsbyrating":
                return new SortWorkItemRatingCommand(engine);
            case "sortworkitemsbytsize":
                return new SortWorkItemSizeCommand(engine);

        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandName));
    }
}


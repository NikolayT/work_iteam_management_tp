package enums;

public enum PriorityType {
    HIGH("High"),
    MEDIUM("Medium"),
    LOW("Low");

    private static final String NOT_A_VALID_PRIORITY = "This is not a valid priority type";

    private String textValue;

    PriorityType(String textValue) {
        this.textValue = textValue;
    }

    public String getTextValue() {
        return textValue;
    }

    public static PriorityType fromString(String text) {
        for (PriorityType pt : PriorityType.values()) {
            if (pt.textValue.equalsIgnoreCase(text)) {
                return pt;
            }
        }

        throw new IllegalArgumentException(NOT_A_VALID_PRIORITY);
    }
}

package enums;

public enum SeverityType {

    CRITICAL("Critical"),
    MAJOR("Major"),
    MINOR("Minor");

    private static final String NOT_A_VALID_SEVERITY = "This is not a valid severity type";

    private String textValue;

    SeverityType(String textValue) {
        this.textValue = textValue;
    }

    public String getTextValue() {
        return textValue;
    }

    public static SeverityType fromString(String text) {
        for (SeverityType st : SeverityType.values()) {
            if (st.textValue.equalsIgnoreCase(text)) {
                return st;
            }
        }

        throw new IllegalArgumentException(NOT_A_VALID_SEVERITY);
    }
}

package enums;

public enum StatusType {
    ACTIVE("Active"),
    FIXED("Fixed"),
    NOT_DONE("Not_Done"),
    IN_PROGRESS("In_Progress"),
    DONE("Done"),
    NEW("New"),
    UNSCHEDULED("Unscheduled"),
    SCHEDULED("Scheduled");

    private static final String NOT_A_VALID_STATUS = "This is not a valid status type";

    private String textValue;

    StatusType(String textValue) {
        this.textValue = textValue;
    }

    public String getTextValue() {
        return textValue;
    }

    public static StatusType fromString(String text) {
        for (StatusType s : StatusType.values()) {
            if (s.textValue.equalsIgnoreCase(text)) {
                return s;
            }
        }

        throw new IllegalArgumentException(NOT_A_VALID_STATUS);
    }
}

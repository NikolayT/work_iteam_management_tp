package enums;

public enum StorySizeType {
    LARGE("Large"),
    MEDIUM("Medium"),
    SMALL("Small");

    private static final String NOT_A_VALID_SIZE = "This is not a valid size";

    private String textValue;

    StorySizeType(String textValue) {
        this.textValue = textValue;
    }

    public String getTextValue() {
        return textValue;
    }

    public static StorySizeType fromString(String text) {
        for (StorySizeType sz : StorySizeType.values()) {
            if (sz.textValue.equalsIgnoreCase(text)) {
                return sz;
            }
        }

        throw new IllegalArgumentException(NOT_A_VALID_SIZE);
    }
}

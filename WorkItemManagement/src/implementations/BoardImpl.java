package implementations;

import contracts.Board;
import contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    private static final int MIN_NAME_LENGTH = 5;
    private static final int MAX_NAME_LENGTH = 10;
    private static final String VALIDATION_FOR_LENGTH = String.format("The name should be between %d and %d symbols",
            MIN_NAME_LENGTH, MAX_NAME_LENGTH);

    private static final String ADD_TO_BOARD_ACTIVITY = "added to the board";

    private String name;
    private List<WorkItem> workItems;
    private List<String> activityHistory;

    public BoardImpl(String name) {
        setName(name);
        workItems = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void createNewWorkItem(WorkItem workItem) {
        activityHistory.add(String.format("%s %s", workItem.getClassName(), ADD_TO_BOARD_ACTIVITY));
        workItems.add(workItem);
    }

    @Override
    public String getBoardInfo() {
        if (workItems.size() == 0) {
            return name + " has no workitems yet";
        }

        StringBuilder boardInfo = new StringBuilder();
        boardInfo.append(String.format("Board Name: %s\n", name));

        for (WorkItem item : workItems) {
            boardInfo.append(item.toString());
            boardInfo.append("\n");
        }

        return boardInfo.toString();
    }

    @Override
    public String showBoardActivity() {
        if (activityHistory.size() == 0) {
            return name + " has no activity yet";
        }

        StringBuilder history = new StringBuilder(name + " activity history:\n");
        for (String activity : activityHistory) {
            history.append(String.format(" %s\n", activity));
        }

        return history.toString();
    }

    @Override
    public List<WorkItem> getWorkItems() {
        return workItems;
    }

    private void setName(String name) {
        if (name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH) {
            throw new IllegalArgumentException(VALIDATION_FOR_LENGTH);
        }
        this.name = name;
    }
}

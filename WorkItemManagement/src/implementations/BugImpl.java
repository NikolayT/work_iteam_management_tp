package implementations;

import base.SubBaseWorkItem;
import contracts.Bug;
import enums.PriorityType;
import enums.SeverityType;
import enums.StatusType;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends SubBaseWorkItem implements Bug {

    private static final String CHANGING_SEVERITY = "Severity of Bug with ID: %s was changed to %s";
    private static final String CLASS_NAME = "Bug";
    private static final String INVALID_STATUS_TYPE = "Invalid bug status type!";

    private List<String> stepsToReproduce;
    private SeverityType severityType;

    public BugImpl(String title, String description, List<String> stepsToReproduce,
                   PriorityType priority, SeverityType severityType) {

        super(title, description, priority, StatusType.ACTIVE);
        setSeverityType(severityType);
        setStepsToReproduce(stepsToReproduce);
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public SeverityType getSeverityType() {
        return severityType;
    }

    @Override
    public String getClassName() {
        return CLASS_NAME;
    }

    @Override
    public void changeSeverity(SeverityType bugSeverityType) {
        setSeverityType(bugSeverityType);
        super.addActivityHistory(String.format(CHANGING_SEVERITY, getID(), severityType));
    }

    @Override
    protected StatusType validateStatusType(StatusType statusType) {
        switch (statusType) {
            case ACTIVE:
                return StatusType.ACTIVE;
            case FIXED:
                return StatusType.FIXED;
            default:
                throw new IllegalArgumentException(INVALID_STATUS_TYPE);
        }
    }

    @Override
    protected String additionalInfo() {
        if (getAssignee() == null) {
            return String.format("Steps to reproduce:\n%s\nPriority: %s\nSeverity: %s\nAssignee: none",
                    listSteps(), getPriority().getTextValue(), severityType.getTextValue());
        } else {
            return String.format("Steps to reproduce:\n%s\nPriority: %s\nSeverity: %s\nAssignee: %s",
                    listSteps(), getPriority().getTextValue(), severityType.getTextValue(), getAssignee().getName());
        }
    }

    private String listSteps() {
        if (stepsToReproduce.size()== 0) return "";
        StringBuilder sb = new StringBuilder();
        for (String step : stepsToReproduce) {
            sb.append(String.format("  %s\n", step));
        }

        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    private void setSeverityType(SeverityType severityType) {
        this.severityType = severityType;
    }

    private void setStepsToReproduce(List<String> stepsToReproduce) {

        this.stepsToReproduce = new ArrayList<>(stepsToReproduce);
    }
}

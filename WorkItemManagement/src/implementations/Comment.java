package implementations;

import contracts.Person;

public class Comment {

    private String content;
    private Person author;

    public Comment(String content, Person author) {
        setContent(content);
        setAuthor(author);
    }

    @Override
    public String toString() {
        return String.format("Author: %s\n Comment: %s", author.getName(), content);
    }

    public String getContent() {
        return content;
    }

    public Person getAuthor() {
        return author;
    }

    private void setContent(String content) {
        this.content = content;
    }

    private void setAuthor(Person author) {
        this.author = author;
    }
}
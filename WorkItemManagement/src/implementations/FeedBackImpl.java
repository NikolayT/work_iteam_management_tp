package implementations;

import contracts.Feedback;
import base.WorkItemBase;
import enums.StatusType;

public class FeedBackImpl extends WorkItemBase implements Feedback {

    private static final int MIN_RATING_SCALE = 0;
    private static final int MAX_RATING_SCALE = 10;

    private static final String CHANGE_RATING = "Rating of Feedback with ID: %s was changed to %d";
    private static final String CLASS_NAME = "Feedback";
    private static final String INVALID_STATUS = "Invalid status type!";

    private int rating;

    public FeedBackImpl(String title, String description, int rating) {
        super(title, description, StatusType.NEW);
        setRating(rating);
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public void changeRating(int rating) {
        setRating(rating);
        super.addActivityHistory(String.format(CHANGE_RATING, getID(), rating));
    }

    @Override
    public String getClassName() {
        return CLASS_NAME;
    }

    @Override
    protected String additionalInfo() {
        return String.format("Rating: %d", rating);
    }

    @Override
    protected StatusType validateStatusType(StatusType statusType) {
        switch (statusType) {
            case NEW:
                return StatusType.NEW;
            case UNSCHEDULED:
                return StatusType.UNSCHEDULED;
            case SCHEDULED:
                return StatusType.SCHEDULED;
            case DONE:
                return StatusType.DONE;
            default:
                throw new IllegalArgumentException(INVALID_STATUS);
        }
    }

    private void setRating(int rating) {
        if (rating < MIN_RATING_SCALE || rating > MAX_RATING_SCALE) {
            throw new IllegalArgumentException(
                    String.format("Rating must be between %d and %d.", MIN_RATING_SCALE, MAX_RATING_SCALE));
        }

        this.rating = rating;
    }
}

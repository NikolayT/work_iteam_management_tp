package implementations;

import java.util.ArrayList;
import java.util.List;

public class IDGenerator {
    private static final String DEFAULT_ID = "0000000001";
    private static final String DEFAULT_VALUE_FOR_FILLING = "0";
    private static List<String> idS = new ArrayList<>();
    private static final long LENGHT_OF_ID = 10;

    public static String generateID() {
        if (idS.size() == 0) {
            idS.add(DEFAULT_ID);
        } else {
            long temp = Long.parseLong(idS.get(idS.size() - 1));
            temp++;
            String stringTemp = String.valueOf(temp);
            idS.add(addZeros(stringTemp));
        }
        return idS.get(idS.size() - 1);
    }

    // this method is used only for test purposes
    public static void clearGenerator() {
        idS.clear();
    }

    private static String addZeros(String string) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= LENGHT_OF_ID - string.length(); i++) {
            sb.append(DEFAULT_VALUE_FOR_FILLING);
        }
        sb.append(string);
        return sb.toString();
    }
}

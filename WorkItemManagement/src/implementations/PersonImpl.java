package implementations;

import base.SubBaseWorkItem;
import contracts.Person;
import contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

public class PersonImpl implements Person {

    private static final int MIN_NAME_LENGTH = 5;
    private static final int MAX_NAME_LENGTH = 15;
    private static final String VALIDATION_FOR_LENGTH = String.format("The name should be between %d and %d symbols",
            MIN_NAME_LENGTH, MAX_NAME_LENGTH);

    private static final String ASSIGN_WORK_ITEM = "Person got an item from the board with title: ";
    private static final String UNASSIGN_WORK_ITEM = "Person set item on the board with title: ";

    private static final String MISSING_WORK_ITEM_OF_PERSON = "%s does not have this work item";

    private String name;
    private List<WorkItem> workItems;
    private List<String> activityHistory;
    private static UniqueNameValidator uniqueNameValidator = new UniqueNameValidator();

    public PersonImpl(String name) {
        setName(name);
        workItems = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    public void addingActivityHistory(String command){
        activityHistory.add(command);
    }

    @Override
    public String assignWorkItem(WorkItem workItem) {

        if (containsWorkItem(workItem.getID())) {
            return String.format("WorkItem with ID: %s already assigned", workItem.getID());
        }

        workItems.add(workItem);
        if (workItem.getClassName().equals("Bug") || workItem.getClassName().equals("Story")) {
            ((SubBaseWorkItem) workItem).assign(this);
        }

        addingActivityHistory(String.format("%s%s", ASSIGN_WORK_ITEM, workItem.getTitle()));

        return String.format("%s%s", ASSIGN_WORK_ITEM, workItem.getTitle());
    }

    @Override
    public String unassignWorkItem(WorkItem workItem) {
        if (workItems.contains(workItem)) {
            workItems.remove(workItem);
            addingActivityHistory(String.format("%s%s", UNASSIGN_WORK_ITEM, workItem.getTitle()));

            return String.format("%s%s", UNASSIGN_WORK_ITEM, workItem.getTitle());
        }

        return String.format(MISSING_WORK_ITEM_OF_PERSON ,name);
    }

    @Override
    public String showPersonActivity() {
        if (activityHistory.size() == 0) {
            return name + " has no activity yet";
        }

        StringBuilder history = new StringBuilder(name + " activity history:\n");
        for (String activity : activityHistory) {
            history.append(activity);
            history.append("\n");
        }

        return history.toString();
    }

    @Override
    public String getName() {
        return name;
    }

    // this method is for test purposes only
    public static void clearNames() {
        uniqueNameValidator.clearNames();
    }

    private void setName(String name) {
        if (name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH) {
            throw new IllegalArgumentException(VALIDATION_FOR_LENGTH);
        }

        uniqueNameValidator.reserveName(name);
        this.name = name;
    }

    private boolean containsWorkItem(String ID) {
        for (WorkItem workItem: workItems) {
            if (workItem.getID().equals(ID)) {
                return true;
            }
        }
        return false;
    }
}

package implementations;

import base.SubBaseWorkItem;
import contracts.Story;
import enums.PriorityType;
import enums.StatusType;
import enums.StorySizeType;

public class StoryImpl extends SubBaseWorkItem implements Story {

    private static final String CHANGE_SIZE = "Size of Story with ID: %s was changed to %s";
    private static final String INVALID_STATUS_TYPE = "Invalid story status type!";
    private static final String CLASS_NAME = "Story";

    private StorySizeType sizeType;

    public StoryImpl(String title, String description, PriorityType priority, StorySizeType sizeType) {

        super(title, description, priority, StatusType.NOT_DONE);
        setSizeType(sizeType);
    }

    @Override
    public StorySizeType getSize() {
        return sizeType;
    }

    @Override
    public void changeSize(StorySizeType storySizeType) {
        setSizeType(storySizeType);
        super.addActivityHistory(String.format(CHANGE_SIZE, getID(), sizeType));
    }

    @Override
    public String getClassName() {
        return CLASS_NAME;
    }

    @Override
    protected StatusType validateStatusType(StatusType statusType) {
        switch (statusType) {
            case NOT_DONE:
                return StatusType.NOT_DONE;
            case IN_PROGRESS:
                return StatusType.IN_PROGRESS;
            case DONE:
                return StatusType.DONE;
            default:
                throw new IllegalArgumentException(INVALID_STATUS_TYPE);
        }
    }

    @Override
    protected String additionalInfo() {
        if (getAssignee() == null) {
            return String.format("Priority: %s\nSize: %s\nAssignee: none",
                    getPriority().getTextValue(), sizeType.getTextValue());
        } else {
            return String.format("Priority: %s\nSize: %s\nAssignee: %s",
                    getPriority().getTextValue(), sizeType.getTextValue(), getAssignee().getName());
        }
    }

    private void setSizeType(StorySizeType sizeType) {
        this.sizeType = sizeType;
    }
}

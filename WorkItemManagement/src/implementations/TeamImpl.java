package implementations;

import contracts.Board;
import contracts.Person;
import contracts.Team;
import contracts.WorkItem;
import exceptions.DublicatedBoardNameException;
import exceptions.DublicatedNameException;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {

    private String name;
    private List<Person> members;
    private List<Board> boards;
    private UniqueNameValidator uniqueBoardValidator;

    private static final String BOARD_NAME_EXCEPTION = "This board name is already used";
    private static final String ADDING_PERSON_TO_TEAM = "was added to team - ";

    public TeamImpl(String name) {
        setName(name);
        members = new ArrayList<>();
        boards = new ArrayList<>();
        uniqueBoardValidator = new UniqueNameValidator();
    }

    public List<Board> getBoards() {
        return boards;
    }

    public List<Person> getMembers() {
        return members;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void addPerson(Person person) {
        members.add(person);
        person.addingActivityHistory(String.format("%s %s%s", person.getName(), ADDING_PERSON_TO_TEAM, this.name));
    }

    @Override
    public String showTeam() {
        return String.format("%s\n%s%s", name, showAllTeamMembers(), showAllTeamBoards());
    }

    @Override
    public void createNewBoard(String boardName) {
        try {
            uniqueBoardValidator.reserveName(boardName);
            BoardImpl newBoard = new BoardImpl(boardName);
            boards.add(newBoard);
        } catch (DublicatedNameException e) {
            throw new DublicatedBoardNameException(BOARD_NAME_EXCEPTION);
        }
    }

    @Override
    public void addWorkItemOnBoard(String boardName, WorkItem workItem) {
        for (Board board : boards) {
            if (board.getName().equals(boardName)) {
                board.createNewWorkItem(workItem);
                return;
            }
        }

        throw new IllegalArgumentException(String.format("%s does not exist", boardName));
    }

    @Override
    public String showTeamActivity() {
        StringBuilder allTeamMembers = new StringBuilder();
        String teamActivityMessage = String.format("%s activity:\n", this.getName());

        for (Person p : members) {
            allTeamMembers.append(p.showPersonActivity());
        }

        if (allTeamMembers.length() == 0) {
            return name + " has no activity yet\n";
        }
        return teamActivityMessage + allTeamMembers.toString();
    }

    @Override
    public String showAllTeamMembers() {
        if (members.size() == 0) {
            return "There are no members added yet.";
        }
        StringBuilder allTeamMembers = new StringBuilder("Team Members:\n");
        for (Person p : members) {
            allTeamMembers.append(String.format(" %s\n", p.getName()));
        }
        return allTeamMembers.toString();
    }

    @Override
    public String showAllTeamBoards() {
        if (boards.size() == 0) {
            return "There are no boards added yet.";
        }
        StringBuilder allTeamBoards = new StringBuilder("Team Boards:\n");
        for (Board b : boards) {
            allTeamBoards.append(b.getBoardInfo());
        }

        return allTeamBoards.toString();
    }

    @Override
    public int getNumberOfTeamMembers() {
        return members.size();
    }

    @Override
    public int getNumberOfTeamBoards() {
        return boards.size();
    }

    private void setName(String name) {
        this.name = name;
    }
}

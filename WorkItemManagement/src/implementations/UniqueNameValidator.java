package implementations;

import exceptions.DublicatedNameException;

import java.util.HashSet;
import java.util.Set;

public class UniqueNameValidator {
    private static final String NAME_USED = "This name is already used" ;
    private Set<String> uniqueNameValidator;

    public UniqueNameValidator() {
        uniqueNameValidator = new HashSet<>();
    }

    public void reserveName(String name) {
        if (uniqueNameValidator.contains(name)) {
            throw new DublicatedNameException(NAME_USED);
        }

        uniqueNameValidator.add(name);
    }

    // this method is for test purposes only
    public void clearNames() {
        uniqueNameValidator.clear();
    }
}
